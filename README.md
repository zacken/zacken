<!--
SPDX-FileCopyrightText: 2024 Marten Ringwelski
SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

> STATUS: In very early development, not stable, but I am confident that no drastic
> major changes will be done to the GraphQL schema. Still, pretty much all of
> zacken is currently work in progress.


# Zacken Analysis Suite
