# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

import json
import pathlib as pl

import attrs
import pytest

from zacken import models as m
from zacken.graphql.schema import schema


def _scalar_or_object_from_value(value, recursive: bool):
    err = ValueError(f"Value {value} which is of unsupported type {type(value)}")
    match value:
        case str() | int() | float() | None:
            return value
        case list() | tuple():
            return tuple(sorted(_scalar_or_object_from_value(v, recursive) for v in value))
        case GraphQlData():
            return value
        case dict():
            if not recursive:
                raise err
            return GraphQlData.from_dict(value, recursive=True)
        case _:
            raise err


@attrs.frozen
class GraphQlData:
    """A type representing any json object that stems from a graphql API.
    In contrast to python dict this class is hashable and, equality and
    less-than are defined."""

    # XXX Provide nice pytest diagnostics
    # https://docs.pytest.org/en/6.2.x/assert.html#defining-your-own-explanation-for-failed-assertions
    data: dict

    @classmethod
    def from_dict(cls, data: dict, recursive: bool = True):
        return cls(
            data={
                key: _scalar_or_object_from_value(value, recursive) for key, value in data.items()
            }
        )

    def __lt__(self, other):
        return self.__hash__() < other.__hash__()

    def __eq__(self, other):
        if not isinstance(other, GraphQlData):
            raise TypeError(f"'==' operator not supported for {type(self)} and {type(other)}")

        return self.__hash__() == other.__hash__()

    def __hash__(self):
        return hash(tuple(sorted(self.data.items())))


def _object_hook(obj):
    return GraphQlData.from_dict(obj, recursive=False)


def _discover_cases() -> dict:
    queries = []
    datas = []
    case_names = []
    for test_case in (pl.Path(__file__).parent / "cases").glob("*/*"):
        data_path = test_case / "data.json"
        query_path = test_case / "query.graphql"

        queries.append(query_path.read_text())
        if data_path.exists():
            with data_path.open() as f:
                data = json.load(
                    f,
                    object_hook=_object_hook,
                )
                datas.append(data)
        else:
            # XXX Remove this
            datas.append(None)

        case_names.append(f"{test_case.parent.name}:{test_case.name}")

    return {
        "argnames": ["query", "data"],
        "argvalues": list(zip(queries, datas)),
        "ids": case_names,
    }


@pytest.mark.parametrize(**_discover_cases())
async def test_queries(graphql_context, query, data):
    if data is None:
        pytest.skip(reason="No data.json")

    result = await schema.execute(
        query,
        context_value=graphql_context,
    )

    assert result.data is not None
    result_data = GraphQlData.from_dict(result.data)
    assert result.errors is None
    assert result_data == data


def test_db(session):
    image = m.FirmwareImage()
    blob = m.BinaryLargeObject(
        size=10,
        ref="ref",
    )
    image.blob = blob
    blob.unpacking_report.firmware_image = image
    blob.firmware_image = image

    session.add(blob)
    session.commit()
