# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

import asyncio

import pytest
import sqlalchemy as sa
import sqlalchemy.engine

from zacken import models as m
from zacken.database import DatabaseSession
from zacken.graphql.context import Context


@pytest.fixture(scope="session")
def _postgres_url(request) -> sa.engine.URL:
    return sa.engine.make_url(
        request.config.getoption("--postgres-url"),
    )


@pytest.fixture(scope="session")
def _minio_url(request) -> str:
    return request.config.getoption("--minio-url")


@pytest.fixture(scope="session")
def _minio_access_key(request) -> str:
    return request.config.getoption("--minio-access-key")


@pytest.fixture(scope="session")
def _minio_secret_key(request) -> str:
    return request.config.getoption("--minio-secret-key")


@pytest.fixture(scope="session")
def event_loop(request):
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="session")
def _dbs(_postgres_url) -> DatabaseSession:
    return DatabaseSession.from_url(
        postgres_url=_postgres_url,
        assign_global=False,
    )


@pytest.fixture(scope="session", autouse=True)
def _database_is_populated(_dbs, _postgres_url):
    with _dbs.Session() as session:
        blob_count = session.scalar(
            sa.select(
                sa.func.count(m.BinaryLargeObject.id),
            ),
        )

    if blob_count > 0:
        return

    raise RuntimeError(
        f"The database at {_postgres_url} is not populated."
        " Please restore the integration test database at 'tests/integration/cases/zacken.tar.gz'"
    )


@pytest.fixture(scope="function")
def session(_dbs):
    """Yields an instance of a zacken.database.DatabaseSession.Session. The returned session
    is inside a transaction and rolled back when the after each test executed."""
    connection = _dbs.engine.connect()
    transaction = connection.begin()
    session = _dbs.Session(
        bind=connection,
    )

    yield session

    session.close()
    transaction.rollback()
    connection.close()


@pytest.fixture(scope="function")
async def async_session(_dbs):
    """Yields an instance of a zacken.database.DatabaseSession.AsyncSession. The returned session
    is inside a transaction and rolled back when the after each test executed."""
    connection = await _dbs.async_engine.connect()
    transaction = await connection.begin()
    async_session = _dbs.AsyncSession(
        bind=connection,
    )

    yield async_session

    await async_session.close()
    await transaction.rollback()
    await connection.close()


@pytest.fixture(scope="function")
async def graphql_context(async_session) -> Context:
    return Context(
        session=async_session,
    )
