# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only


def pytest_addoption(parser):
    parser.addoption(
        "--postgres-url",
        action="store",
        default="postgresql://postgres:postgres@localhost:5432/zacken-test",
        help="Same as 'zacken.postgres-url' in the config. Used by tests in tests/integration.",
    )
    parser.addoption(
        "--minio-url",
        action="store",
        default="http://localhost:9000",
        help="Same as 'zacken.minio-url' in the config. Used by tests in tests/integration.",
    )
    parser.addoption(
        "--minio-access-key",
        action="store",
        default="minioadmin",
        help="Same as 'zacken.minio-access-key' in the config. Used by tests in tests/integration.",
    )
    parser.addoption(
        "--minio-secret-key",
        action="store",
        default="minioadmin",
        help="Same as 'zacken.minio-secret-key' in the config. Used by tests in tests/integration.",
    )
