# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

import pytest
import sqlalchemy as sa
import sqlalchemy.orm
import strawberry

from zacken.graphql import filter


class Base(sa.orm.DeclarativeBase):
    pass


class ModelA(Base):
    __tablename__ = "model_a"
    id: sa.orm.Mapped[int] = sa.orm.mapped_column(primary_key=True)


class ModelB(Base):
    __tablename__ = "model_b"
    id: sa.orm.Mapped[int] = sa.orm.mapped_column(primary_key=True)


def test_add_requires_model():
    with pytest.raises(filter.FilterError):

        @filter.add()
        @strawberry.input
        class GraphqlType:
            id: int


def test_add_requires_strawberry_input():
    with pytest.raises(filter.FilterError):

        @filter.add()
        class GraphqlType:
            id: int
