# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

from typing import Annotated

import pytest
import sqlalchemy as sa
import strawberry
import strawberry as stb

from zacken.graphql import factory


class Base(sa.orm.DeclarativeBase):
    pass


class ModelA(Base):
    __tablename__ = "model_a"
    id: sa.orm.Mapped[int] = sa.orm.mapped_column(primary_key=True)


class ModelB(Base):
    __tablename__ = "model_b"
    id: sa.orm.Mapped[int] = sa.orm.mapped_column(primary_key=True)


def test_add_requires_model():
    with pytest.raises(factory.FactoryError):

        @factory.add()
        @strawberry.type
        class GraphqlType:
            pass


def test_add_requires_strawberry_type():
    with pytest.raises(factory.FactoryError):

        @factory.add()
        class GraphqlType:
            Model = ModelA


def test_add_requires_sqlalchemy_model():
    with pytest.raises(factory.FactoryError):

        class NoModel:
            pass

        @factory.add()
        @strawberry.type
        class GraphqlType:
            Model = NoModel


def test_add_accepts_lazy_types():
    @factory.add()
    @strawberry.type
    class GraphqlType:
        Model = ModelA

        id: Annotated["ModelB", stb.lazy(".factory")]

    factory.resolve()
