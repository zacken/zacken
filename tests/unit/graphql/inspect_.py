# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

import pytest
import sqlalchemy as sa
import sqlalchemy.orm

from zacken.graphql import inspect


class Base(sa.orm.DeclarativeBase):
    pass


class Model(Base):
    __tablename__ = "model"
    id: sa.orm.Mapped[int] = sa.orm.mapped_column(primary_key=True)


def test_get_model_or_raise_no_model():
    class GraphqlType:
        pass

    with pytest.raises(ValueError):
        inspect.get_model_or_raise(GraphqlType)


def test_get_model_or_raise_wrong_model():
    class NoModel:
        pass

    class GraphqlType:
        Model = NoModel

    with pytest.raises(TypeError):
        inspect.get_model_or_raise(GraphqlType)
