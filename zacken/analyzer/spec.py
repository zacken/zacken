# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

import functools
import importlib
import typing

import attrs

from zacken.types import Ref


@attrs.frozen
class Spec:
    #: The plugin's name to be used in the GraphQL schema.
    #: Note that the name is coverted to camelCase.
    name: str = attrs.field()
    #: A short description of the plugin
    description: str = attrs.field()
    #: The module the 'analyze' function resides in.
    analyze_module_path: str = attrs.field()
    #: The primary database model. Note that you are not restricted to a single
    #: model, referenced models are also added.
    SqlalchemyRoot: type = attrs.field()
    #: The primary graphql type.
    # XXX Think about if we want the user to decorate this with factory and filter or not
    StrawberryRoot: type = attrs.field()
    #: Types that are not referenced by StrawberryRoot directly.
    #: See the 'types' paremeter of strawberry.Schema for more information.
    strawberry_types: list[type] = attrs.field(default=attrs.Factory(list))

    @property
    def python_name(self):
        return self.name.replace("-", "_")

    @functools.cached_property
    def analyze_fn(self) -> typing.Callable:
        # Lazy to allow it to depend on zacken things like the config.
        analyze_module = importlib.import_module(self.analyze_module_path)
        return analyze_module.analyze

    def analyze(self, ref: Ref):
        return self.analyze_fn(ref)
