# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

import celery
import sqlalchemy as sa
import sqlalchemy.orm

from zacken import models as m
from zacken.database import dbs
from zacken.worker import analyzer_tasks as tasks


class Scheduler:
    def analyze_firmware_image(self, firmware_id):
        with dbs.Session() as session:
            firmware = session.get(
                m.FirmwareImage,
                firmware_id,
                options=[
                    sa.orm.selectinload(m.FirmwareImage.blobs),
                ],
            )
            if firmware is None:
                raise ValueError(f"FirmwareImage with id {firmware_id} does not exist")

            task_group = celery.group(
                tasks.analyze_blob.s(blob.id, "software-signature") for blob in firmware.blobs
            )
            result = task_group.apply_async()
            result.forget()
