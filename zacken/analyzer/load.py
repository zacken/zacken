# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

"""Import this module to load all analyzers"""

from .analyzer import name2spec


def _load():
    from . import software_signature

    spec = software_signature.spec
    name2spec[spec.name] = spec


_load()
