# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

from zacken import analyzer

from . import graphql as g
from . import models as m

spec = analyzer.Spec(
    name="software-signature",
    description="Uses yara signatures to identify software.",
    SqlalchemyRoot=m.SoftwareSignature,
    StrawberryRoot=g.SoftwareSignature,
    analyze_module_path=__name__ + ".analyze",
)

__all__ = ["spec"]
