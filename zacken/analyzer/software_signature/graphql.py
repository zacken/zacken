# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

import typing

import strawberry

from zacken.graphql import factory, filter

from . import models as m


@filter.add()
@factory.add()
@strawberry.type
class SoftwareSignature:
    Model = m.SoftwareSignature

    id: strawberry.ID
    linux: typing.Optional["LinuxSignatures"]


@filter.add()
@factory.add()
@strawberry.type
class LinuxSignatures:
    Model = m.LinuxSignatures

    id: int
    # XXX Do not change capitalize these names in
    linux_banner: int | None
    linux_proc_banner: int | None
    rtc_ydays: int | None
    rtc_days_in_month: int | None
    _ctype: int | None
    hex_asc: int | None
    hex_asc_upper: int | None
    band_table: int | None

    banner_str: str | None
