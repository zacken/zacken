# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

import sqlalchemy as sa
import sqlalchemy.orm
from sqlalchemy.orm import Mapped, mapped_column, relationship

from zacken import models as m


class LinuxSignatures(m.Base):
    __tablename__ = "linux_signatures"

    id: Mapped[int] = mapped_column(
        primary_key=True,
        index=True,
    )
    linux_banner: Mapped[int | None]
    linux_proc_banner: Mapped[int | None]
    rtc_ydays: Mapped[int | None]
    rtc_days_in_month: Mapped[int | None]
    _ctype: Mapped[int | None]
    hex_asc: Mapped[int | None]
    hex_asc_upper: Mapped[int | None]
    band_table: Mapped[int | None]

    banner_str: Mapped[str | None]


class SoftwareSignature(m.Base, m.BinaryLargeObjectAnalysisMixin):
    __tablename__ = "software_signature"

    id: Mapped[int] = mapped_column(
        primary_key=True,
        index=True,
    )
    linux_id: Mapped[int] = mapped_column(
        sa.ForeignKey("linux_signatures.id"),
        index=True,
        nullable=True,
    )
    linux: Mapped["LinuxSignatures"] = relationship()
