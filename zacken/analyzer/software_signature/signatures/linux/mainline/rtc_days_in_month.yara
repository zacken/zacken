// SPDX-FileCopyrightText: 2024 Marten Ringwelski
// SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
//
// SPDX-License-Identifier: AGPL-3.0-only

// In Linux v6.5 this is defined in
// drivers/rtc/lib.c:16
//
// First introduced in `c58411e95d7f5062dedd1a3064af4d359da1e633` which was
// released in v2.6.17.
// Moved in `36e14f5fdfdf7cec8887b7ff69cd9bb5051ecf62`
// Only compiled if CONFIG_RTC_LIB is set.
// ```sh
// git log --patch -- ./drivers/rtc/lib.c
// git log --patch -- ./drivers/rtc/rtc-lib.c
// git log --patch -- ./drivers/Makefile | rg rtc
// ```sh

rule rtc_days_in_month {
    strings:
        $rtc_days_in_month = { 1f 1c 1f 1e 1f 1e 1f 1f 1e 1f 1e 1f }
    condition:
        $rtc_days_in_month
}
