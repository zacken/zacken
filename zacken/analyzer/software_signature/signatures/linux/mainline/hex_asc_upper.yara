// SPDX-FileCopyrightText: 2024 Marten Ringwelski
// SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
//
// SPDX-License-Identifier: AGPL-3.0-only

// In Linux v6.5 this is defined in
// lib/hexdump.c:14
//
// This was first introduced in `c26d436cbf7a9549ec1073480a2e3f0d3f64e02d`
// which was released in v3.12.

rule hex_asc_upper {
    strings:
        $hex_asc_upper = "0123456789ABCDEF\x00"
    condition:
        $hex_asc_upper
}
