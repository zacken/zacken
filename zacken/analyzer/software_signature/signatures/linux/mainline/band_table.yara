// SPDX-FileCopyrightText: 2024 Marten Ringwelski
// SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
//
// SPDX-License-Identifier: AGPL-3.0-only

// In Linux v6.5 this is defined in
// fs/fcntl.c:691
//
// First introduced in the first git commit.
// It was then changed in `a9a08845e9acbd224e4ee466f5c1275ed50054e8` which was
// released in v4.16. The commit message says that things SHOULD be the same
// on most architectures.
// and stayed unchanged since then.
//
// ```sh
// git log --patch ./fs/fcntl.c
// git log --patch ./fs/Makefile | rg -B 1 fcntl.o
// git log --patch ./include/uapi/asm-generic/siginfo.h  | rg NSIGPOLL
// ```

rule band_table {
    strings:
        $band_table_le = {41 00 00 00 04 03 00 00 41 04 00 00 08 00 00 00 82 00 00 00 18 00 00 00}
        $band_table_be = {00 00 00 41 00 00 03 04 00 00 04 41 00 00 00 08 00 00 00 82 00 00 00 18}
    condition:
        $band_table_le or $band_table_be
}
