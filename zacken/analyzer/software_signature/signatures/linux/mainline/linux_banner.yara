// SPDX-FileCopyrightText: 2024 Marten Ringwelski
// SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
//
// SPDX-License-Identifier: AGPL-3.0-only

// In Linux v6.5 this is defined in
// init/version-timestamp.c:28
//
// It first appears in the first linux git commit and is not modified in any
// releases.
// Cannot be excluded except for patching.
// ```sh
// git log --patch ./init/version-timestamp.c
// git log --patch ./init/version.c
// git log --patch ./init/Makefile | rg version.o
// ```

rule linux_banner {
    strings:
        // While this looks a bit obscure this is a regex matching
        // Note that compilers can include parentheses or more general: everything
        // "Linux version " UTS_RELEASE " (" LINUX_COMPILE_BY "@" LINUX_COMPILE_HOST ") (" LINUX_COMPILER ") " UTS_VERSION "\n";
        $linux_banner = /Linux version (.*) \(.*@.*\) \(.*\) .*\n\x00/
    condition:
        $linux_banner
}
