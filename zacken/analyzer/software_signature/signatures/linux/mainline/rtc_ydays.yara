// SPDX-FileCopyrightText: 2024 Marten Ringwelski
// SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
//
// SPDX-License-Identifier: AGPL-3.0-only

// In Linux v6.5 this is defined in
// drivers/rtc/lib.c:20

// First introduced in `8232212e0b4ee4eb3e407f5a9b098f6377820164` which was
// released in v2.6.18.
// Only compiled if CONFIG_RTC_LIB is set.
rule rtc_ydays {
    strings:
        // XXX does not work always because of endianness
        $rtc_ydays = "\x00\x00\x1f\x00;\x00Z\x00x\x00\x97\x00\xb5\x00\xd4\x00\xf3\x00\x11\x010\x01N\x01m\x01\x00\x00\x1f\x00<\x00[\x00y\x00\x98\x00\xb6\x00\xd5\x00\xf4\x00\x12\x011\x01O\x01n\x01"

    condition:
        $rtc_ydays
}
