// SPDX-FileCopyrightText: 2024 Marten Ringwelski
// SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
//
// SPDX-License-Identifier: AGPL-3.0-only

// In Linux v6.5 this is defined in
// init/version.c:35
//
// First introduced in `3eb3c740f51c2126b53c2dde974c1c57e634aa7b` which was
// released in v2.6.20.

rule linux_proc_banner {
    strings:
        // Note that compilers can include parentheses or more general: everything
        $linux_proc_banner = /%s version %s \(.*@.*\) \(.*\) %s\n\x00/
    condition:
        $linux_proc_banner
}
