// SPDX-FileCopyrightText: 2024 Marten Ringwelski
// SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
//
// SPDX-License-Identifier: AGPL-3.0-only

// In Linux v6.5 this is defined in
// lib/hexdump.c:14
//
// This was first introduced in `3fc957721d18c93662f7d4dab455b80f53dd2641`
// which was released in v2.6.26

rule hex_asc {
    strings:
        $hex_asc = "0123456789abcdef\x00"
    condition:
        $hex_asc
}
