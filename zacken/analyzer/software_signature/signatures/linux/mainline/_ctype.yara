// SPDX-FileCopyrightText: 2024 Marten Ringwelski
// SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
//
// SPDX-License-Identifier: AGPL-3.0-only

// In Linux v6.5 this is defined in
// lib/ctype.c:12
//
// It first appears in the first linux git commit and is not modified in any
// releases.
// Cannot be excluded except for patching.
// ```sh
// git log --patch ./lib/ctype.c
// git log --patch ./lib/Makefile | rg ctype.o
// ```

rule _ctype {
    strings:
        $_ctype = {
            08 08 08 08 08 08 08 08
            08 28 28 28 28 28 08 08
            08 08 08 08 08 08 08 08
            08 08 08 08 08 08 08 08
            a0 10 10 10 10 10 10 10
            10 10 10 10 10 10 10 10
            04 04 04 04 04 04 04 04
            04 04 10 10 10 10 10 10
            10 41 41 41 41 41 41 01
            01 01 01 01 01 01 01 01
            01 01 01 01 01 01 01 01
            01 01 01 10 10 10 10 10
            10 42 42 42 42 42 42 02
            02 02 02 02 02 02 02 02
            02 02 02 02 02 02 02 02
            02 02 02 10 10 10 10 08
            00 00 00 00 00 00 00 00
            00 00 00 00 00 00 00 00
            00 00 00 00 00 00 00 00
            00 00 00 00 00 00 00 00
            a0 10 10 10 10 10 10 10
            10 10 10 10 10 10 10 10
            10 10 10 10 10 10 10 10
            10 10 10 10 10 10 10 10
            01 01 01 01 01 01 01 01
            01 01 01 01 01 01 01 01
            01 01 01 01 01 01 01 10
            01 01 01 01 01 01 01 02
            02 02 02 02 02 02 02 02
            02 02 02 02 02 02 02 02
            02 02 02 02 02 02 02 10
            02 02 02 02 02 02 02 02
        }
    condition:
        $_ctype
}
