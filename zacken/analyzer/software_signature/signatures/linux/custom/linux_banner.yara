// SPDX-FileCopyrightText: 2024 Marten Ringwelski
// SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
//
// SPDX-License-Identifier: AGPL-3.0-only

// This is the same as the linux_banner signature but matches only the start of
// it. The reason for this is that some kernels had some postfix after the
// whole linux banner. So in combination with another signature this should be good.
rule linux_version {
    strings:
        $linux_version = "Linux version "
    condition:
        $linux_version
}

// This is exactly the linux_banner signature, but without the trailing nullbyte
rule linux_banner_unterminated {
    strings:
        $linux_banner_unterminated = /Linux version (.*) \(.*@.*\) \(.*\) .*\n/
        $linux_banner_terminated = /Linux version (.*) \(.*@.*\) \(.*\) .*\n\x00/
    condition:
        $linux_banner_unterminated and not $linux_banner_terminated
}
