<!--
SPDX-FileCopyrightText: 2024 Marten Ringwelski
SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Tips and Tricks
Compile a tinyconfig vmlinux to get the value for complicated things
(e.g. structs, large arrays).
Somehow compiling only the object file does not work.

Take endanness into account for constants larger than one byte.

Grep the git log for changes to the file the signature is defined in.
Also grep the makefiles to check if they are always compiled in.
