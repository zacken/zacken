# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

import pathlib as pl

import yara

from zacken import storage
from zacken.types import Ref

from . import models as m

mainline_rules = yara.compile(str(pl.Path(__file__).parent / "signatures/linux/mainline.cat"))


def _linux_signatures_from_matches(matches: list[yara.Match]) -> m.LinuxSignatures:
    kwargs = {}
    for match in matches:
        # XXX Add tests that verify that our signatures match the type m.LinuxSignatures
        assert len(match.strings) == 1, f"{match.rule} signature has more than one string"
        match_count = len(match.strings[0].instances)
        kwargs[match.rule] = match_count if match_count != 0 else None

    return m.LinuxSignatures(**kwargs)


def analyze(ref: Ref) -> m.SoftwareSignature:
    with storage.fget(ref) as path:
        matches = mainline_rules.match(str(path))
    return m.SoftwareSignature(
        linux=_linux_signatures_from_matches(matches),
    )
