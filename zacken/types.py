# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

# A reference identifying a BinaryLargeObject
# XXX We should document that BinaryLargeObject.id in the graphql api is just
# a convenience thing if you want only integer keys.
# XXX(python3.12): Use type statement
Ref = str
