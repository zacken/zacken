# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

import sqlalchemy as sa
import sqlalchemy.orm
from sqlalchemy.orm import Mapped, mapped_column, relationship

from zacken import models as m


class UnpackerReportMixin:
    status: Mapped["m.SchedulingStatus"] = mapped_column(
        sa.Enum(m.SchedulingStatus),
        nullable=False,
        index=True,
    )

    @sa.orm.declared_attr
    def unpacking_report(cls) -> Mapped["m.UnpackingReport"]:
        return relationship(
            back_populates=cls.__tablename__,
        )

    archive_id: Mapped[int] = mapped_column(
        sa.ForeignKey(
            "archive.id",
        ),
        index=True,
        nullable=True,
    )

    @sa.orm.declared_attr
    def archive(cls) -> Mapped["m.Archive"]:
        return relationship(
            foreign_keys=cls.archive_id,
        )
