# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

import functools
import importlib
import typing

import attrs

from zacken import models as m


@attrs.frozen
class Spec:
    name: str
    description: str
    SqlalchemyRoot: type
    StrawberryRoot: type
    strawberry_types: list[type]
    unpack_module_path: str

    @property
    def python_name(self):
        return self.name.replace("-", "_")

    @functools.cached_property
    def unpack_fn(self) -> typing.Callable:
        # Lazy to allow it to depend on zacken things like the config.
        unpack_module = importlib.import_module(self.unpack_module_path)
        return unpack_module.analyze

    def unpack(self, blob: m.BinaryLargeObject):
        return self.unpack_fn(blob)
