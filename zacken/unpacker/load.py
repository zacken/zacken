# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

from .unpacker import name2spec


def _load():
    from . import unblob

    spec = unblob.spec
    name2spec[spec.name] = spec


_load()
