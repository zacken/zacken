# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

from zacken import unpacker

from . import graphql as g
from . import models as m

spec = unpacker.Spec(
    name="unblob",
    description=("A leightweight wrapper around the unblob unpacker."),
    unpack_module_path=__name__ + ".unpack",
    SqlalchemyRoot=m.Unblob,
    StrawberryRoot=g.Unblob,
    strawberry_types=[],
)
