# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

import json
import pathlib as pl
import subprocess as sp
from typing import Generator

import attrs
import cattrs
import networkx as nx
import unblob.models
import unblob.report

from zacken import cfg, storage
from zacken import models as zm
from zacken.models.models import BinaryLargeObject
from zacken.models.tree_archive_builder import (
    TreeArchiveBuilder,
    TreeArchiveBuilderNode,
)

from . import models as m

TRANSFORMING_ARCHIVE_HANDLERS = ["gzip"]

_converter = cattrs.Converter()


def _unpack_singleton_iterator(it):
    try:
        item = next(it)
    except StopIteration:
        return None

    try:
        _ = next(it)
        return None
    except StopIteration:
        return item


def _blob_from_path(path: str | pl.Path) -> zm.BinaryLargeObject:
    path = pl.Path(path)
    ref = storage.fput(path)
    return zm.BinaryLargeObject(
        ref=ref,
        size=path.stat().st_size,
    )


@attrs.define
class UnblobTaskGraph:
    """The unblob task graph is a annotated graph that is generated from
    the output of unblob's `--report`.
    Nodes are of type unblob.models.Task and edges represent the 'subtask'
    relationship defined by the 'subtasks' attribute of unblob.models.Task.
    In addition to this direct representation of the graph defined by the
    report json, the graph's nodes and edges are annotated. Node annotations
    are:
    - 'report': An instance of ReportCollection
    - 'path': The relative path of the tasks filesystem node. E.g. if you use
       set the --extract-dir to /data/output, the 'path' attribute is relative
       to this.
    Edge attributes are:
    - 'chunk': An instance of unblob.report.ChunkReport if the edge source is a
      carved archive and the destination is the task resulting from the 'chunk'.

    The following describes the strucutre of the graph in detail while
    highlighting potential footguns.
    Unblob only makes a difference between known and unknown archives.
    They do not have a notion of archive kind, like Zacken has.
    In unblob terms, there are nine possible ({compressed, known, unknown}x{known, unknown, compressed}),
    parent-child archive type cases, all of which have a different representation
    in the graph.
    XXX Document this properly
    Some notes:
    - Chunks that are an archive are have a unblob.report.ChunkReport and are
      direct children of wherever they came from.
    - Sometimes gzip puts a 'gzip.uncompressed' file on disk and sometimes uses
      a filename.

    """

    # XXX Have a look at every place that modified archive_task2blob
    # and make this easier and remove footguns.

    # XXX Eliminate footgun of using unblob.models.Task.path instead of the
    # 'path' attribute.
    # Maybe, we should specify that the unblob tasks should not be used and
    # only the node attributes may be used.

    root: unblob.models.Task
    graph: nx.DiGraph

    def _get_predecessor_edge_chunk(
        self, task: unblob.models.Task
    ) -> unblob.report.ChunkReport | None:
        predecessors = self.graph.predecessors(task)
        predecessor = _unpack_singleton_iterator(predecessors)
        if predecessor is None:
            return None

        return self.graph.edges[predecessor, task].get("chunk", None)

    def get_archive_kind(self, task: unblob.models.Task) -> zm.ArchiveKind | None:
        if self.is_unknown_archive(task):
            return zm.ArchiveKind.Unknown
        elif self.is_tree_archive(task):
            return zm.ArchiveKind.Tree
        elif self.is_transforming_archive(task):
            return zm.ArchiveKind.Transforming
        else:
            return None

    def is_unknown_archive(self, task: unblob.models.Task) -> bool:
        # The input is any file
        report: ReportCollection = self.graph.nodes[task]["report"]
        if not report.stat.is_file:
            return False
        if len(report.unknown_chunks) == 0 and len(report.chunks) == 0:
            return False

        if chunk := _unpack_singleton_iterator(iter(report.unknown_chunks + report.chunks)):
            # Somehow unblob reports chunks for things it does not even unpack.
            # For example when unpacking libatomic.a any contained object file
            # will hit this.
            # As unblob emits chunks that cover the whole file, one chunk
            # means the chunk is the whole file.
            assert chunk.size == report.stat.size
            return False

        return True

    def peel_file_task(self, task: unblob.models.Task) -> unblob.models.Task | None:
        # XXX This is because of terrible unblob api.
        # A file that is of a recognized archive format will have a subtask
        # which is the files '_extract' directory.
        # For example for fielsystem_core.squashfs (a file in a tree archive)
        # they create a subtask with the _extract postfix.
        # For unknown archives they do not do this.
        # This function peels of this first layer (i.e. the file) and returns the task
        # that is the '_extract' directory.
        report: ReportCollection = self.graph.nodes[task]["report"]
        assert report.stat.is_file
        if not (subtask := _unpack_singleton_iterator(self.graph.successors(task))):
            return None

        task_path = self.graph.nodes[task]["path"]
        subtask_path = self.graph.nodes[subtask]["path"]
        if str(task_path) + "_extract" == str(subtask_path):
            return subtask

        return None

    def is_tree_archive(self, task: unblob.models.Task) -> bool:
        # The input task is the '_extract' directory task
        # So for a file like filesystem.image (squashfs) this is the extract thing,
        # not the file itself.
        # This is because unblob adds extra tasks for files, but not for chunks!
        report: ReportCollection = self.graph.nodes[task]["report"]
        if not report.stat.is_dir:
            return False

        chunk = self._get_predecessor_edge_chunk(task)
        if not chunk:
            return False

        return chunk.handler_name not in TRANSFORMING_ARCHIVE_HANDLERS

    def is_transforming_archive(self, task: unblob.models.Task) -> bool:
        # The input task is the '_extract' directory task of the thing whereever
        # the gzip comes from.
        # So if it is carved, then the '_extract' dir comes from there.
        # If the gzip is the tree root, the root will have a subtask called '_exctact'.
        report: ReportCollection = self.graph.nodes[task]["report"]
        if not report.stat.is_dir:
            return False

        chunk = self._get_predecessor_edge_chunk(task)
        if not chunk:
            return False

        ret = chunk.handler_name in TRANSFORMING_ARCHIVE_HANDLERS
        # Transforming archives have exactly one subtask.
        # E.g. for gzip it is called gzip.uncompressed
        assert not ret or _unpack_singleton_iterator(self.graph.successors(task))
        return ret

    def _iter_archives_impl(self, task: unblob.models.Task, archive: unblob.models.Task):
        for child in self.graph.neighbors(task):
            if self.get_archive_kind(child) is not None:
                yield child
                yield from self._iter_archives_impl(child, child)
            else:
                yield from self._iter_archives_impl(child, archive)

    def iter_archives(self):
        """Iterate all nodes that are archives"""
        yield from self._iter_archives_impl(self.root, self.root)

    def _iter_tree_archive_edges_impl(self, node: unblob.models.Task):
        for child in self.graph.neighbors(node):
            if self.is_tree_archive(child) or self.is_transforming_archive(child):
                # Both tree archives and transforming archives are represented
                # by a task that is an '_extract' directory.
                # We do not care for these as they are not part of the tree archive.
                # Unknown archives are files so they do not matter to us.
                continue

            yield node, child
            # In unblob a directory contains all its contents as tasks.
            # Thus we recursivly call this for child here.
            yield from self._iter_tree_archive_edges_impl(child)

    def iter_tree_archive_edges(
        self,
        archive_root: unblob.models.Task,
    ) -> Generator[tuple[unblob.models.Task, unblob.models.Task], None, None]:
        """Iterates all edges that are part of a single tree archive
        starting at archive_root."""
        if not self.is_tree_archive(archive_root):
            raise ValueError(f"{archive_root} is not a TreeArchive")

        yield from self._iter_tree_archive_edges_impl(archive_root)

    def iter_unknown_archive_chunks(
        self,
        archive_root: unblob.models.Task,
    ) -> Generator[unblob.report.ChunkReport | unblob.report.UnknownChunkReport, None, None]:
        """Iterates all nodes that are part of the unknown archive starting
        at archive_root"""
        if not self.is_unknown_archive(archive_root):
            raise ValueError(f"{archive_root} is not an UnknownArchive")

        report: ReportCollection = self.graph.nodes[archive_root]["report"]

        yield from report.chunks
        yield from report.unknown_chunks


@attrs.frozen
class ReportCollection:
    chunks: list[unblob.report.ChunkReport]
    unknown_chunks: list[unblob.report.UnknownChunkReport]
    hash: unblob.report.HashReport
    stat: unblob.report.StatReport

    @classmethod
    def from_reports(
        cls,
        reports: list[unblob.report.Report],
    ) -> "ReportCollection":
        kwargs = {
            "hash": None,
            "stat": None,
            "chunks": [],
            "unknown_chunks": [],
        }
        for report in reports:
            match report:
                case unblob.report.HashReport():
                    assert kwargs["hash"] is None
                    kwargs["hash"] = report
                case unblob.report.StatReport():
                    assert kwargs["stat"] is None
                    kwargs["stat"] = report
                case unblob.report.ChunkReport():
                    kwargs["chunks"].append(report)
                case unblob.report.UnknownChunkReport():
                    kwargs["unknown_chunks"].append(report)

        return ReportCollection(**kwargs)


@attrs.frozen
class ArchiveFactory:
    """A factory that creates zacken archives from unblob tasks."""

    #: The firmware image that the archives should be assigned to
    # XXX This would be much nicer if sqlalchemy did this for us.
    firmware_image: zm.FirmwareImage
    task_graph: UnblobTaskGraph
    # The prefix that should be added to the relative paths in
    # the node attribute 'path'.
    path_prefix: pl.Path
    # A mapping form all archive tasks to a zacken binary large object.
    # Used to map archive_tasks to blobs.
    archive_task2blob: dict[unblob.models.Task, zm.BinaryLargeObject] = attrs.field(factory=dict)

    def _node_prefixed_path(self, node: unblob.models.Task):
        path = self.task_graph.graph.nodes[node]["path"]
        return self.path_prefix / path

    def unknown_archive_from_task(
        self,
        archive_task: unblob.models.Task,
    ) -> zm.UnknownArchive:
        report = self.task_graph.graph.nodes[archive_task]["report"]
        path = self._node_prefixed_path(archive_task)

        content = path.read_bytes()
        zacken_chunks = []
        blob_id2blob = {}
        for unblob_chunk in report.chunks:
            ref = storage.put(content[unblob_chunk.start_offset : unblob_chunk.end_offset])
            blob = BinaryLargeObject(
                ref=ref,
                size=unblob_chunk.size,
            )
            blob.firmware_image = self.firmware_image
            blob.unpacking_report.firmware_image = self.firmware_image
            blob_id2blob[unblob_chunk.id] = blob
            zacken_chunks.append(
                zm.Chunk(
                    offset=unblob_chunk.start_offset,
                    length=unblob_chunk.size,
                    blob=blob,
                    firmware_image=self.firmware_image,
                ),
            )

        for successor in self.task_graph.graph.successors(archive_task):
            chunk = self.task_graph.graph.edges[archive_task, successor]["chunk"]
            self.archive_task2blob[successor] = blob_id2blob[chunk.id]

        for unblob_unknown_chunk in report.unknown_chunks:
            ref = storage.put(
                content[unblob_unknown_chunk.start_offset : unblob_unknown_chunk.end_offset]
            )
            blob = BinaryLargeObject(
                ref=ref,
                size=unblob_unknown_chunk.size,
            )
            blob.firmware_image = self.firmware_image
            blob.unpacking_report.firmware_image = self.firmware_image
            zacken_chunks.append(
                zm.Chunk(
                    offset=unblob_unknown_chunk.start_offset,
                    length=unblob_unknown_chunk.size,
                    blob=blob,
                    firmware_image=self.firmware_image,
                ),
            )

        return zm.UnknownArchive(
            chunks=zacken_chunks,
            firmware_image=self.firmware_image,
        )

    def tree_archive_from_task(self, archive_task: unblob.models.Task) -> zm.TreeArchive:
        builder = TreeArchiveBuilder()

        task2builder_node_id = {archive_task: None}
        for parent, child in self.task_graph.iter_tree_archive_edges(archive_task):
            builder_node = TreeArchiveBuilderNode(
                node=self._file_system_node_from_task(child),
                parent_id=task2builder_node_id[parent],
            )
            id = builder.add(builder_node)
            task2builder_node_id[child] = id

        archive = builder.finalize()
        archive.firmware_image = self.firmware_image
        return archive

    def transforming_archive_from_task(self, task: unblob.models.Task) -> zm.TransformingArchive:
        if not self.task_graph.is_transforming_archive(task):
            raise ValueError(f"{task} is not a TransformingArchive")

        # This is the "gzip.uncompressed" thingy
        successor = _unpack_singleton_iterator(self.task_graph.graph.successors(task))
        assert successor is not None

        transformed_path = self._node_prefixed_path(successor)
        transformed = _blob_from_path(transformed_path)
        transformed.firmware_image = self.firmware_image
        transformed.unpacking_report.firmware_image = self.firmware_image
        # Unblob is so wired.
        # A gzip that contains a known archive has the tree as below.
        # A gzip that contains an unknown archive misses the _extract thing
        # and directly.
        match self.task_graph.get_archive_kind(successor):
            case zm.ArchiveKind.Unknown:
                self.archive_task2blob[successor] = transformed
        if transformed_extract_dir_task := self.task_graph.peel_file_task(successor):
            # Unblob is so wired. Consistent, but wired.
            # In case of a gzip that is carved the tree looks like the following.
            # unknown-archive.bin
            # - gzip_extract
            #   - gzip.uncompressed [sucessor]
            #     - gzip.uncompressed_extract [transformed_extract_dir_task]
            self.archive_task2blob[transformed_extract_dir_task] = transformed

        return zm.TransformingArchive(
            transformed=transformed,
            firmware_image=self.firmware_image,
        )

    def _file_system_node_from_task(self, task: unblob.models.Task):
        report = self.task_graph.graph.nodes[task]["report"]
        path = self._node_prefixed_path(task)
        name = task.path.name
        if report.stat.is_dir:
            return zm.Directory(
                name=name,
                firmware_image=self.firmware_image,
            )
        elif report.stat.is_file:
            blob = _blob_from_path(path)
            blob.firmware_image = self.firmware_image
            blob.unpacking_report.firmware_image = self.firmware_image

            if file_archive_task := self.task_graph.peel_file_task(task):
                self.archive_task2blob[file_archive_task] = blob
            else:
                self.archive_task2blob[task] = blob
            return zm.File(
                name=name,
                blob=blob,
                firmware_image=self.firmware_image,
            )
        elif report.stat.is_link:
            # XXX Add things that are MaliciousSymlinkRemoved
            return zm.SymbolicLink(
                name=name,
                dest=str(path.readlink()),
                firmware_image=self.firmware_image,
            )
        else:
            assert False


def task_graph_from_report_path(report_path: pl.Path | str) -> UnblobTaskGraph:
    report_path = pl.Path(report_path)
    process_result = _process_result_from_report(report_path)

    return task_graph_from_process_result(process_result)


def task_graph_from_process_result(
    process_result: unblob.models.ProcessResult,
) -> UnblobTaskGraph:
    graph = nx.DiGraph()

    for result in process_result.results:
        task = result.task
        report = ReportCollection.from_reports(result.reports)
        blob_id2chunk = {chunk.id: chunk for chunk in report.chunks}

        for subtask in result.subtasks:
            if chunk := blob_id2chunk.get(subtask.blob_id, None):
                graph.add_edge(task, subtask, chunk=chunk)
            else:
                graph.add_edge(task, subtask)

        graph.nodes[task]["report"] = report

    # XXX Special handeling for root here that makes it consistent with
    # the other "_extract" things.
    root = process_result.results[0].task
    assert root.depth == 0
    assert root.blob_id == ""

    # Take only the name as the firmware may be arbitrarily deep in /data/input
    graph.nodes[root]["path"] = root.path.name
    # Skip the root as it is not part of the output of unblob
    for result in process_result.results[1:]:
        task = result.task
        graph.nodes[task]["path"] = task.path.relative_to("/data/output/")

    return UnblobTaskGraph(
        root=root,
        graph=graph,
    )


def _structure_report(data, type) -> unblob.report.Report:
    typename = data["__typename__"]
    subtype = getattr(unblob.report, typename)
    return cattrs.structure_attrs_fromdict(data, subtype)


_converter.register_structure_hook(unblob.report.Report, _structure_report)


def _process_result_from_report(path: pl.Path) -> unblob.models.ProcessResult:
    with path.open() as f:
        data = json.load(f)

    return unblob.models.ProcessResult(
        results=_converter.structure(data, list[unblob.models.TaskResult]),
    )


def _create_models(blob: zm.BinaryLargeObject, archive_factory):
    task2archive = {}
    for archive_task in archive_factory.task_graph.iter_archives():
        match archive_factory.task_graph.get_archive_kind(archive_task):
            case zm.ArchiveKind.Tree:
                archive = archive_factory.tree_archive_from_task(archive_task)
                task2archive[archive_task] = archive
            case zm.ArchiveKind.Unknown:
                archive = archive_factory.unknown_archive_from_task(archive_task)
                task2archive[archive_task] = archive
            case zm.ArchiveKind.Transforming:
                archive = archive_factory.transforming_archive_from_task(archive_task)
                task2archive[archive_task] = archive

    archive_task2blob = archive_factory.archive_task2blob
    task_graph = archive_factory.task_graph
    if (peeled := task_graph.peel_file_task(task_graph.root)) is not None:
        archive_task2blob[peeled] = blob
    else:
        archive_task2blob[task_graph.root] = blob

    for task, archive in task2archive.items():
        blob = archive_task2blob[task]
        blob.unpacking_report.unblob = m.Unblob(
            archive=archive,
            status=zm.SchedulingStatus.Done,
        )


def unpack(blob: zm.BinaryLargeObject):
    # XXX Avoid potential footguns and avoid loading
    assert blob.ref is not None
    assert blob.firmware_image is not None
    assert blob.unpacking_report is not None

    with storage.fget(blob.ref) as blob_path, storage.tmpfs.directory() as output_dir:
        input_dir = blob_path.parent
        # XXX Maybe the storage module should guarantee that this is true
        assert str(input_dir) == cfg.gets("zacken-worker", "storage-cache-dir")
        report_path = output_dir / "report.json"
        sp.run(
            [
                "docker",
                "run",
                "--rm",
                "-v",
                f"{output_dir}:/data/output",
                "-v",
                f"{input_dir}:/data/input",
                "ghcr.io/onekey-sec/unblob:latest",
                f"/data/input/{blob_path.name}",
                "--report",
                f"/data/output/{report_path.name}",
            ],
            check=True,
            stdout=sp.DEVNULL,
            stderr=sp.DEVNULL,
        )

        task_graph = task_graph_from_report_path(report_path)
        archive_factory = ArchiveFactory(
            task_graph=task_graph,
            firmware_image=blob.firmware_image,
            path_prefix=output_dir,
        )

        _create_models(blob, archive_factory)
