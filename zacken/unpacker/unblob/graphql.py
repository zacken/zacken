# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

import strawberry

from zacken.graphql import factory, filter
from zacken.graphql.query import Archive, SchedulingStatus

from . import models as m


@filter.add()
@factory.add()
@strawberry.type
class Unblob:
    Model = m.Unblob

    id: strawberry.ID
    status: SchedulingStatus
    archive: Archive
