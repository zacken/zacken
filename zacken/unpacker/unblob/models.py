# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

from sqlalchemy.orm import Mapped, mapped_column

from zacken import models as m
from zacken.unpacker.models import UnpackerReportMixin


class Unblob(m.Base, UnpackerReportMixin):
    __tablename__ = "unblob"

    id: Mapped[int] = mapped_column(
        primary_key=True,
        index=True,
    )
