# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

from zacken import cfg

imports = ("zacken.worker.analyzer_tasks",)

task_serializer = "json"
broker_url = cfg.gets("zacken-worker", "rabbitmq-url")

result_serializer = "pickle"
result_backend = cfg.gets("zacken-worker", "redict-url")

broker_connection_retry_on_startup = False
