# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

import sqlalchemy as sa

from zacken import models as m
from zacken.analyzer.analyzer import name2spec
from zacken.database import dbs

from .app import app


@app.task
def analyze_blob(blob_id: int, analyzer: str):
    """Analyze a singele blob with an analyzer."""
    spec = name2spec[analyzer]
    del analyzer

    with dbs.Session() as session:
        blob = session.get(
            m.BinaryLargeObject,
            ident=blob_id,
            options=[
                sa.orm.selectinload(m.BinaryLargeObject.analysis_report),
                sa.orm.load_only(
                    m.BinaryLargeObject.ref,
                    m.BinaryLargeObject.id,
                ),
            ],
        )
        if blob is None:
            raise RuntimeError(f"There is no blob with id {id}.")

        assert hasattr(
            blob.analysis_report, spec.python_name
        ), f"XXX convert to unit test: {spec.name} {blob.analysis_report}"
        assert hasattr(blob.analysis_report, f"{spec.python_name}_status")

        model = spec.analyze(blob.ref)
        setattr(
            blob.analysis_report,
            spec.python_name,
            model,
        )
        setattr(
            blob.analysis_report,
            f"{spec.python_name}_status",
            m.SchedulingStatus.Done,
        )
        # XXX This is needed but ugly
        session.add(blob.analysis_report)
        session.add(model)
        session.commit()


@app.task
def analyze_blobs(ids: list[int], analyzer: str):
    """Analyze multiple blobs with an analyzer. Should be preferred over
    analyze_blob if the analyzer is known to be very fast to prevent io
    overhead."""
    raise NotImplementedError("XXX Smarter scheduling")
