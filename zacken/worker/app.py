# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

import celery.signals
from celery import Celery

import zacken.database

from . import celeryconfig

dbs = zacken.database.get_or_set_global()

app = Celery()
app.config_from_object(celeryconfig)


# tldr: sqlalchemy engines cannot be shared through processes
#
# [1] https://www.yangster.ca/post/not-the-same-pre-fork-worker-model/
# [2] https://github.com/apache/superset/issues/10530
# [3] https://docs.sqlalchemy.org/en/20/core/pooling.html#using-connection-pools-with-multiprocessing-or-os-fork
@celery.signals.worker_process_init.connect
def dispose_engine(**kwargs):
    dbs.engine.dispose(close=False)
