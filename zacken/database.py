# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

import os

import attrs
import sqlalchemy as sa
import sqlalchemy.engine
from sqlalchemy.ext.asyncio import (
    async_sessionmaker,
    create_async_engine,
)

dbs: "DatabaseSession" = None

_echo = os.getenv("ZACKEN_DEBUG_SQL", None) is not None


@attrs.frozen
class DatabaseSession:
    # XXX What exactly does this class encapsulate?
    engine: sa.Engine
    Session: sa.orm.Session
    async_engine: sa.ext.asyncio.AsyncEngine
    AsyncSession: sa.ext.asyncio.AsyncSession

    @classmethod
    def from_config(cls, assign_global: bool = True) -> "DatabaseSession":
        from zacken import cfg

        return cls.from_url(
            postgres_url=cfg.gets("zacken", "postgres-url"),
        )

    @classmethod
    def from_url(
        cls,
        postgres_url: str,
        assign_global: bool = True,
    ) -> "DatabaseSession":
        default_url = sa.engine.make_url(postgres_url)
        psycopg_url = default_url.set(
            drivername="postgresql+psycopg",
        )
        asyncpg_url = default_url.set(
            drivername="postgresql+asyncpg",
        )

        engine_ = sa.create_engine(
            psycopg_url,
            echo=_echo,
        )
        Session_ = sa.orm.sessionmaker(engine_)

        async_engine_ = create_async_engine(
            asyncpg_url,
            echo=_echo,
        )
        AsyncSession_ = async_sessionmaker(async_engine_, expire_on_commit=False)

        ret = cls(
            engine=engine_,
            Session=Session_,
            async_engine=async_engine_,
            AsyncSession=AsyncSession_,
        )

        if assign_global:
            global dbs  # noqa: PLW0603
            if dbs is not None:
                raise ValueError(
                    "The global 'zacken.database.dbs' is already set."
                    " Please use 'get_or_set_global', or set 'assign_global' to False."
                )
            dbs = ret

        return ret


def get_or_set_global() -> "DatabaseSession":
    if dbs is None:
        _ = DatabaseSession.from_config(assign_global=True)

    return dbs
