# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

# Import these here to load all analyzer/unpacker models.
# If we directly importet dem in zacken.models.models or zacken.analyzer,
# it would cause a circular dependency.
import zacken.analyzer.load  # noqa: F401, I001
import zacken.unpacker.load  # noqa: F401, I001
import zacken.models  # noqa: F401, I001
