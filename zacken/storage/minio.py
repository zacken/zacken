# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

import urllib.parse

import minio

from zacken import cfg

# XXX Invalidate this client in celery
# Musn't we?
_minio_url = urllib.parse.urlparse(
    cfg.gets("zacken", "minio-url"),
)
client = minio.Minio(
    _minio_url.netloc,
    access_key=cfg.gets("zacken", "minio-access-key"),
    secret_key=cfg.gets("zacken", "minio-secret-key"),
    secure=_minio_url.scheme == "https",
)
bucket = "zacken"


if not client.bucket_exists(bucket):
    try:
        client.make_bucket(bucket)
    except:  # noqa: E722
        raise ValueError(
            f"The specified minio instance at {_minio_url} does not have a bucked named {bucket}"
            " and the bucket could not be created."
        )
