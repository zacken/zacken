# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only


class StorageError(Exception):
    pass


class RefNotFoundError(StorageError):
    """Raised when a ref was requested, that is not stored in minio"""


class NotEnoughSpaceError(StorageError):
    """Raised when there is not enough space to store the requested blob in
    cache due to high cache usage.
    See also CacheTooSmallError"""


class UnknownStorageError(StorageError):
    """A storage error that has no special handeling.
    See the causing exception for more information."""


class CacheTooSmallError(StorageError):
    """Raised when the cache is too small to fit a blob even
    if no other blobs were cached."""
