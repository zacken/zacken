# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

import contextlib
import os
import pathlib as pl
import shutil
import tempfile
import typing

from filelock import UnixFileLock

from zacken import cfg

tmpfs_dir = cfg.gets("zacken-worker", "tmpfs-dir")


@contextlib.contextmanager
def file() -> typing.Generator[pl.Path, None, None]:
    fd, path = tempfile.mkstemp(dir=tmpfs_dir)
    os.close(fd)

    lock_path = path + ".lock"
    lock = UnixFileLock(
        lock_path,
        timeout=0,
    )

    lock.acquire()
    yield pl.Path(path)
    os.unlink(path)
    os.unlink(lock_path)
    lock.release()


@contextlib.contextmanager
def directory() -> typing.Generator[pl.Path, None, None]:
    path = tempfile.mkdtemp(dir=tmpfs_dir)
    lock_path = path + ".lock"
    lock = UnixFileLock(
        lock_path,
        timeout=0,
    )

    lock.acquire()
    yield pl.Path(path)
    shutil.rmtree(path)
    os.unlink(lock_path)
    lock.release()


def collect_garbage():
    raise NotImplementedError
