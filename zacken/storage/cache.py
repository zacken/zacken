# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

# Caching is not supported by MinIO [1].
# As we want to cache in memory anyways, this module is a leightweight wrapper
# around the MiniIO api which caches in memory.
#
# [1]: https://github.com/minio/minio/discussions/15669


import contextlib
import errno
import os
import pathlib as pl
import time
import typing
import uuid

import filelock
import minio
from filelock import UnixFileLock

from zacken import cfg
from zacken.types import Ref

from .errors import NotEnoughSpaceError, RefNotFoundError, UnknownStorageError
from .minio import bucket, client

_path: pl.Path = pl.Path(cfg.gets("zacken-worker", "storage-cache-dir"))
_size: int


def fetch(ref: Ref, wait_for_space=True) -> pl.Path:
    """Fetches a blob by its ref from the minio server and returns a path to a
    file of undefined name whose contents are the blob."""
    dest = _path / ref

    TIMEOUT = 10
    lock_path = pl.Path(str(dest) + ".lock")
    lock = UnixFileLock(
        lock_path,
        timeout=TIMEOUT,
    )

    # XXX Implement proper caching and do not deadlock
    # for files larger than the cache.
    while True:
        # Prevent multiple processes from downloading the same file.
        # Note that aquire automatically creates the file for us.
        lock.acquire()
        try:
            if not dest.exists():
                client.fget_object(
                    bucket,
                    ref,
                    str(dest),
                )
            link = pl.Path(str(dest) + f".{uuid.uuid4().hex}")
            os.link(dest, link)
            return link
        except filelock.Timeout:
            # XXX In that case we should probably just try to donwload it ourselves
            # Still, we should handle incredibly large files that actually take long.
            raise UnknownStorageError(
                f"Another process took at least {TIMEOUT} seconds to download the blob."
            )
        except minio.S3Error as e:
            if e.code != "NoSuchKey":
                raise UnknownStorageError from e
            raise RefNotFoundError(f"There is no object with ref: {ref}") from e
        except OSError as e:
            if e.errno != errno.ENOSPC:
                raise UnknownStorageError from e
            # XXX Use stat here to find out if the blobs size and if it could
            # fit in cache.
            if not wait_for_space:
                raise NotEnoughSpaceError(f"Not enough space to store {ref}")
            time.sleep(1)
        finally:
            # XXX Why do we need this?!
            try:
                lock_path.unlink()
                lock.release()
            except Exception:
                pass


@contextlib.contextmanager
def reference(ref) -> typing.Generator[pl.Path, None, None]:
    path = fetch(ref)

    # While it is pretty unlikely that a file returned by fetch is used by
    # multiple processes as it uses uuid4, we ensure this using another lock.
    # Coincidentally, this also allwos us to check this lock in garbage
    # collection.
    lock_path = pl.Path(str(path) + ".lock")

    lock = UnixFileLock(
        lock_path,
        timeout=0,
    )

    lock.acquire()

    try:
        yield path
    finally:
        path.unlink()
        lock_path.unlink()
        lock.release()


def garbage_collect():
    raise NotImplementedError("XXX Implement garbage collection")
