# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

import contextlib
import hashlib
import io
import pathlib as pl
import typing

import minio

from zacken.types import Ref

from . import cache
from .errors import UnknownStorageError
from .minio import bucket, client


def _object_exists(ref: Ref) -> bool:
    try:
        _ = client.stat_object(bucket, ref)
        return True
    except minio.S3Error as e:
        if e.code != "NoSuchKey":
            raise UnknownStorageError from e
        return False


@contextlib.contextmanager
def get(ref: Ref) -> typing.Generator[io.BufferedReader, None, None]:
    """Retuns a buffered reader that corresponds to the given ref's data.
    Raises RefNotFoundError for non-existing refs."""
    # XXX Actually, we do not need to care about preventing deletion of the
    # backing file, since file descriptors are not invalidated on removal.
    with cache.reference(ref) as path:
        with open(path, "rb") as blob:
            yield blob


@contextlib.contextmanager
def fget(ref: Ref) -> typing.Generator[pl.Path, None, None]:
    """Retuns the path to a that corresponds to the given ref's data."""
    with cache.reference(ref) as path:
        yield path


def fput(path: pl.Path | str) -> Ref:
    """Stores the given file as blob and returns its ref."""
    path = pl.Path(path)

    m = hashlib.sha256()
    with path.open("rb") as b:
        buf = b.read()
        m.update(buf)

    ref = m.hexdigest()

    if _object_exists(ref):
        return ref

    try:
        _ = client.put_object(
            bucket_name=bucket,
            object_name=ref,
            data=io.BytesIO(buf),
            length=len(buf),
        )
    except Exception as e:
        raise UnknownStorageError(f"Could not store buffer with ref {ref}.") from e

    return ref


def put(buf: bytes) -> Ref:
    """Stores the given buffer as blob and returns its ref."""
    m = hashlib.sha256()
    m.update(buf)
    ref = m.hexdigest()

    if _object_exists(ref):
        return ref

    try:
        _ = client.put_object(
            bucket_name=bucket,
            object_name=ref,
            data=io.BytesIO(buf),
            length=len(buf),
        )
    except Exception as e:
        raise UnknownStorageError(f"Could not store buffer with ref {ref}.") from e

    return ref
