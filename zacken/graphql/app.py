# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

from fastapi import Depends, FastAPI
from strawberry.fastapi import GraphQLRouter

import zacken.database

from .context import Context
from .schema import schema

dbs = zacken.database.get_or_set_global()


async def context_dependency() -> Context:
    async with dbs.AsyncSession() as session:
        yield Context(
            session=session,
        )


async def get_context(
    custom_context=Depends(context_dependency),
):
    return custom_context


graphql_app = GraphQLRouter(
    schema,
    context_getter=get_context,
)

app = FastAPI()
app.include_router(graphql_app, prefix="/graphql")
