# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

import strawberry

from . import factory, filter, query

filter.resolve()
factory.resolve()

schema = strawberry.Schema(
    query=query.Query,
    types=[
        # Children of Archive
        query.UnknownArchive,
        query.TreeArchive,
        query.TransformingArchive,
        # Children of FileSystemNode
        query.File,
        query.Directory,
        query.SymbolicLink,
    ],
)
