# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

"""Inspection of strawberry types and related types"""

import sqlalchemy as sa
from strawberry.type import get_object_definition
from strawberry.types.types import StrawberryObjectDefinition


def get_model_or_raise(T: type):
    """If the given type has an attribute 'Model' which refers to an sqlalchemy
    mapped class, return it.
    Otherwise, raise a ValueError if it does not exist and a TypeError if it is not
    an sqlalchemy model."""
    if not (Model := getattr(T, "Model", None)):
        raise ValueError(
            f"{T} must have an atribute 'Model' which refers to the sqlalchemy model type."
        )

    try:
        inspector = sa.inspect(Model)  # noqa: F841
    except sa.exc.NoInspectionAvailable:
        raise TypeError(f"{T} has a model type {Model} which is not a sqlalchemy model")

    return Model


def get_module_or_raise(T: type) -> str:
    """Get the module the given type is defined in.
    Raises value error, if the type is not defined in a module."""
    module_path = T.__module__
    if module_path is None:
        raise ValueError(f"Type {T} has not module.")

    # XXX Make this work
    # module = importlib.import_module(module_path)
    # if not hasattr(module, T.__name__):
    #    raise ValueError(
    #        f"Type {T} is not exposed in its module {module_path}."
    #        " Is the type wrapped inside a function or class?"
    #    )
    # XXX Check that the exposed types is actually the correct one

    return module_path


def get_definition_or_raise(T: type) -> StrawberryObjectDefinition:
    """Like strawberry.type.get_object_definition([...], strict=True) but
    raises a ValueError instead of a TypeError."""
    if not (definition := get_object_definition(T, strict=False)):
        raise ValueError(
            f"Type {T} is not a strawberry.type." "Is it decorated with @strawberry.type?"
        )

    return definition
