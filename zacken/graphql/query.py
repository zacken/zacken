# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

from typing import Annotated, Optional

import sqlalchemy as sa
import strawberry
from strawberry.types import Info
from strawberry.types.nodes import SelectedField

from zacken import models as m
from zacken.types import Ref

from . import factory, filter, sdl

# XXX In the current design there is no way to filter for File or SymbolicLink
# things. Maybe we should just expose them as another 'get' thing.


def _raise_not_implemented():
    raise NotImplementedError


def _get_self_selected_field_from_info(info: Info) -> SelectedField:
    for selected_field in info.selected_fields:
        if not isinstance(selected_field, SelectedField):
            continue

        if selected_field.name == info.field_name:
            return selected_field

    assert False, "strawberry error"


ArchiveKind = filter.add()(strawberry.enum(m.ArchiveKind))
FileSystemNodeKind = filter.add()(strawberry.enum(m.FileSystemNodeKind))
SchedulingStatus = filter.add()(strawberry.enum(m.SchedulingStatus))


@sdl.describe_type
@strawberry.type
class Query:
    @sdl.describe_field("Query")
    @strawberry.field
    async def get_firmware_images(
        self,
        info: Info,
        filter: Optional[
            Annotated["FirmwareImageFilter", strawberry.lazy(".query")]  # noqa: F821
        ] = None,
    ) -> list["FirmwareImage"]:
        session = info.context.session
        selected_field = _get_self_selected_field_from_info(info)
        factory = FirmwareImage.Factory.from_selections(
            selected_field.selections,
        )

        stmt = sa.select(
            m.FirmwareImage,
        ).distinct()

        if filter is not None:
            stmt = filter.apply(stmt)

        if options := factory.load_options():
            stmt = stmt.options(*options)

        scalars = await session.scalars(stmt)
        return [factory.produce(model) for model in scalars]

    @sdl.describe_field("Query")
    @strawberry.field
    async def get_blob_analysis_reports(
        self,
        info: Info,
        filter: Optional[
            Annotated[
                "BinaryLargeObjectAnalysisReportFilter", strawberry.lazy(".query")  # noqa: F821
            ]
        ] = None,
    ) -> list["BinaryLargeObjectAnalysisReport"]:
        session = info.context.session
        selected_field = _get_self_selected_field_from_info(info)
        factory = BinaryLargeObjectAnalysisReport.Factory.from_selections(
            selected_field.selections,
        )

        stmt = sa.select(
            m.BinaryLargeObjectAnalysisReport,
        ).distinct()

        if filter is not None:
            stmt = filter.apply(stmt)
        if options := factory.load_options():
            stmt = stmt.options(*options)

        scalars = await session.scalars(stmt)
        return [factory.produce(model) for model in scalars]

    @sdl.describe_field("Query")
    @strawberry.field
    async def get_devices(
        self,
        info: Info,
        filter: Optional[Annotated["DeviceFilter", strawberry.lazy(".query")]] = None,  # noqa: F821
    ) -> list["Device"]:
        session = info.context.session
        selected_field = _get_self_selected_field_from_info(info)
        factory = BinaryLargeObjectAnalysisReport.Factory.from_selections(
            selected_field.selections,
        )

        stmt = sa.select(
            m.Device,
        ).distinct()
        if options := factory.load_options():
            stmt = stmt.options(*options)

        scalars = await session.scalars(stmt)
        return [factory.produce(model) for model in scalars]


@filter.add()
@factory.add()
@sdl.describe_type
@strawberry.type
class Device:
    Model = m.Device

    id: strawberry.ID = strawberry.field(
        description=sdl.description_for("Device.id"),
    )

    get_firmware_images: list["FirmwareImage"] | None = strawberry.field(
        description=sdl.description_for("Device.getFirmwareImages"),
        resolver=_raise_not_implemented,
    )


@filter.add()
@factory.add()
@sdl.describe_type
@strawberry.type
class FirmwareImage:
    Model = m.FirmwareImage

    id: strawberry.ID = strawberry.field(
        description=sdl.description_for("FirmwareImage.id"),
    )
    blob: "BinaryLargeObject" = strawberry.field(
        description=sdl.description_for("FirmwareImage.blob"),
    )
    device: Optional["Device"] = strawberry.field(
        description=sdl.description_for("FirmwareImage.device"),
    )

    @sdl.describe_field("FirmwareImage")
    @strawberry.field
    async def get_blobs(
        self,
        info: Info,
        filter: Optional[
            Annotated["BinaryLargeObjectFilter", strawberry.lazy(".query")]  # noqa: F821
        ] = None,
    ) -> list["BinaryLargeObject"]:
        session = info.context.session
        selected_field = _get_self_selected_field_from_info(info)
        factory = BinaryLargeObject.Factory.from_selections(
            selected_field.selections,
        )

        stmt = (
            sa.select(
                m.BinaryLargeObject,
            )
            .where(
                m.BinaryLargeObject.firmware_image_id == self.id,
            )
            .distinct()
        )

        if filter is not None:
            stmt = filter.apply(stmt)

        if options := factory.load_options():
            stmt = stmt.options(*options)

        scalars = await session.scalars(stmt)

        return [factory.produce(model) for model in scalars]

    @sdl.describe_field("FirmwareImage")
    @strawberry.field
    async def get_archives(
        self,
        info: Info,
        filter: Optional[Annotated["ArchiveFilter", strawberry.lazy(".query")]] = None,  # noqa: F821
    ) -> list["Archive"]:
        session = info.context.session
        selected_field = _get_self_selected_field_from_info(info)
        factory = Archive.Factory.from_selections(selected_field.selections)

        stmt = (
            sa.select(
                m.Archive,
            )
            .where(
                m.Archive.firmware_image_id == self.id,
            )
            .distinct()
        )

        if filter is not None:
            stmt = filter.apply(stmt)

        if options := factory.load_options():
            stmt = stmt.options(*options)

        scalars = await session.scalars(stmt)

        return [factory.produce(model) for model in scalars]

    @sdl.describe_field("FirmwareImage")
    @strawberry.field
    async def get_file_system_nodes(
        self,
        info: Info,
        filter: Optional[
            Annotated["FileSystemNodeFilter", strawberry.lazy(".query")]  # noqa: F821
        ] = None,
    ) -> list["FileSystemNode"]:
        session = info.context.session
        selected_field = _get_self_selected_field_from_info(info)
        factory = FileSystemNode.Factory.from_selections(selected_field.selections)

        stmt = (
            sa.select(
                m.FileSystemNode,
            )
            .where(
                m.FileSystemNode.firmware_image_id == self.id,
            )
            .distinct()
        )

        if filter is not None:
            stmt = filter.apply(stmt)

        if options := factory.load_options():
            stmt = stmt.options(*options)

        scalars = await session.scalars(stmt)

        return [factory.produce(model) for model in scalars]

    @sdl.describe_field("FirmwareImage")
    @strawberry.field
    async def get_chunks(
        self,
        info: Info,
        filter: Optional[Annotated["ChunkFilter", strawberry.lazy(".query")]] = None,  # noqa: F821
    ) -> list["Chunk"] | None:
        session = info.context.session
        selected_field = _get_self_selected_field_from_info(info)
        factory = Chunk.Factory.from_selections(selected_field.selections)

        stmt = (
            sa.select(
                m.Chunk,
            )
            .where(
                m.Chunk.firmware_image_id == self.id,
            )
            .distinct()
        )

        if filter is not None:
            stmt = filter.apply(stmt)

        if options := factory.load_options():
            stmt = stmt.options(*options)

        scalars = await session.scalars(stmt)

        return [factory.produce(model) for model in scalars]


@filter.add()
@factory.add()
@sdl.describe_type
@strawberry.type
class BinaryLargeObject:
    Model = m.BinaryLargeObject

    id: strawberry.ID = strawberry.field(
        description=sdl.description_for("BinaryLargeObject.id"),
    )
    ref: Ref = strawberry.field(
        description=sdl.description_for("BinaryLargeObject.ref"),
    )
    size: int = strawberry.field(
        description=sdl.description_for("BinaryLargeObject.size"),
    )
    unpacking_report: "UnpackingReport" = strawberry.field(
        description=sdl.description_for("BinaryLargeObject.unpackingReport"),
    )
    analysis_report: "BinaryLargeObjectAnalysisReport" = strawberry.field(
        description=sdl.description_for("BinaryLargeObject.analysisReport"),
    )


@filter.add()
@factory.add()
@sdl.describe_type
@strawberry.type
class UnpackingReport:
    Model = m.UnpackingReport

    id: strawberry.ID = strawberry.field(
        description=sdl.description_for("UnpackingReport.id"),
    )
    blob: "BinaryLargeObject" = strawberry.field(
        description=sdl.description_for("UnpackingReport.blob"),
    )

    unblob: Optional[
        Annotated[
            "Unblob",  # noqa: F821
            strawberry.lazy("zacken.unpacker.unblob.graphql"),
        ]
    ] = strawberry.field(
        description=sdl.description_for("UnpackingReport.unblob"),
    )


@filter.add()
@factory.add()
@strawberry.interface
class Archive:
    Model = m.Archive

    id: strawberry.ID = strawberry.field(
        description=sdl.description_for("Archive.id"),
    )
    kind: "ArchiveKind" = strawberry.field(
        description=sdl.description_for("Archive.kind"),
    )


@filter.add(
    overwrite=True,
)
@factory.add(
    overwrite=True,
)
@sdl.describe_type
@strawberry.type
class TreeArchive(Archive):
    Model = m.TreeArchive

    @strawberry.field
    async def get_nodes(
        self,
        info: Info,
        filter: Optional[
            Annotated["FileSystemNodeFilter", strawberry.lazy(".query")]  # noqa: F821
        ] = None,
    ) -> list["FileSystemNode"]:
        session = info.context.session
        selected_field = _get_self_selected_field_from_info(info)
        factory = FileSystemNode.Factory.from_selections(selected_field.selections)

        stmt = (
            sa.select(
                m.FileSystemNode,
            )
            .where(
                m.FileSystemNode.tree_archive_id == self.id,
            )
            .distinct()
        )

        if filter is not None:
            stmt = filter.apply(stmt)

        if options := factory.load_options():
            stmt = stmt.options(*options)

        scalars = await session.scalars(stmt)
        return [factory.produce(model) for model in scalars]


@filter.add()
@factory.add()
@strawberry.interface
class FileSystemNode:
    Model = m.FileSystemNode

    id: strawberry.ID = strawberry.field(
        description=sdl.description_for("FileSystemNode.id"),
    )
    kind: "FileSystemNodeKind" = strawberry.field(
        description=sdl.description_for("FileSystemNode.kind"),
    )
    name: str = strawberry.field(
        description=sdl.description_for("FileSystemNode.name"),
    )
    path: str = strawberry.field(
        description=sdl.description_for("FileSystemNode.path"),
    )


@filter.add(
    overwrite=True,
)
@factory.add(
    overwrite=True,
)
@sdl.describe_type
@strawberry.type
class File(FileSystemNode):
    Model = m.File

    blob: "BinaryLargeObject" = strawberry.field(
        description=sdl.description_for("File.blob"),
    )


@filter.add(
    overwrite=True,
)
@factory.add(
    overwrite=True,
)
@sdl.describe_type
@strawberry.type
class Directory(FileSystemNode):
    Model = m.Directory

    @sdl.describe_field("Directory")
    @strawberry.field
    async def get_children(
        self,
        info: Info,
        filter: Optional[
            Annotated["FileSystemNodeFilter", strawberry.lazy(".query")]  # noqa: F821
        ] = None,
    ) -> list["FileSystemNode"]:
        session = info.context.session
        selected_field = _get_self_selected_field_from_info(info)
        factory = FileSystemNode.Factory.from_selections(
            selected_field.selections,
        )

        parent = sa.orm.aliased(m.FileSystemNode)
        stmt = (
            sa.select(
                m.FileSystemNode,
            )
            .join(
                parent,
                m.FileSystemNode.parent,
            )
            .where(
                parent.id == self.id,
            )
            .distinct()
        )

        if filter is not None:
            stmt = filter.apply(stmt)

        if options := factory.load_options():
            stmt = stmt.options(*options)

        scalars = await session.scalars(stmt)
        return [factory.produce(model) for model in scalars]


@filter.add(
    overwrite=True,
)
@factory.add(
    overwrite=True,
)
@sdl.describe_type
@strawberry.type
class SymbolicLink(FileSystemNode):
    Model = m.SymbolicLink

    dest: str = strawberry.field(
        description=sdl.description_for("SymbolicLink.dest"),
    )


@filter.add(
    overwrite=True,
)
@factory.add(
    overwrite=True,
)
@sdl.describe_type
@strawberry.type
class UnknownArchive(Archive):
    Model = m.UnknownArchive

    @sdl.describe_field("UnknownArchive")
    @strawberry.field
    async def get_chunks(
        self,
        info: Info,
        filter: Optional[Annotated["ChunkFilter", strawberry.lazy(".query")]] = None,  # noqa: F821
    ) -> list["Chunk"]:
        session = info.context.session
        selected_field = _get_self_selected_field_from_info(info)
        factory = Chunk.Factory.from_selections(
            selected_field.selections,
        )

        stmt = (
            sa.select(
                m.Chunk,
            )
            .where(
                m.Chunk.unknown_archive_id == self.id,
            )
            .distinct()
        )

        if filter is not None:
            stmt = filter.apply(stmt)

        if options := factory.load_options():
            stmt = stmt.options(*options)

        scalars = await session.scalars(stmt)

        return [factory.produce(model) for model in scalars]


@filter.add()
@factory.add(
    overwrite=True,
)
@sdl.describe_type
@strawberry.type
class Chunk:
    Model = m.Chunk

    id: strawberry.ID = strawberry.field(
        description=sdl.description_for("Chunk.id"),
    )
    blob: "BinaryLargeObject" = strawberry.field(
        description=sdl.description_for("Chunk.blob"),
    )
    offset: "int" = strawberry.field(
        description=sdl.description_for("Chunk.offset"),
    )
    length: "int" = strawberry.field(
        description=sdl.description_for("Chunk.length"),
    )
    unknown_archive: "UnknownArchive" = strawberry.field(
        description=sdl.description_for("Chunk.unknownArchive"),
    )


@filter.add(
    overwrite=True,
)
@factory.add(
    overwrite=True,
)
@sdl.describe_type
@strawberry.type
class TransformingArchive(Archive):
    Model = m.TransformingArchive

    transformed: BinaryLargeObject = strawberry.field(
        description=sdl.description_for("TransformingArchive.transformed"),
    )


@filter.add()
@factory.add()
@sdl.describe_type
@strawberry.type
class BinaryLargeObjectAnalysisReport:
    Model = m.BinaryLargeObjectAnalysisReport

    id: strawberry.ID

    blob: BinaryLargeObject = strawberry.field(
        description=sdl.description_for("BinaryLargeObjectAnalysisReport.blob"),
    )
    software_signature_status: Optional["SchedulingStatus"] = strawberry.field(
        description=sdl.description_for("BinaryLargeObjectAnalysisReport.softwareSignatureStatus"),
    )
    software_signature: Optional[
        Annotated[
            "SoftwareSignature",  # noqa: F821
            strawberry.lazy("zacken.analyzer.software_signature.graphql"),
        ]
    ] = strawberry.field(
        description=sdl.description_for("BinaryLargeObjectAnalysisReport.softwareSignature"),
    )
