# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

import abc
import importlib
import types
import typing
from typing import Annotated, Generic, TypeVar

import attrs
import sqlalchemy as sa
import strawberry
from strawberry.type import has_object_definition
from strawberry.types.types import StrawberryObjectDefinition

from . import inspect

ScalarFilter = typing.Union[
    "IntIdentifierFilter",
    "IntFilter",
    "StringFilter",
    "StringRegexFilter",
]

V = TypeVar("V")


class TypeFilter(Generic[V], metaclass=abc.ABCMeta):
    """Base class for all filters for strawberry.type's.
    Filters can filter for all fields of the given type, with arbitrarily deep nesting.
    To mark as field as required to be not null, mark it as null, but do not set the filter.
    Use filter.add to add create and add a filter to an existing strawberry.type."""

    @abc.abstractmethod
    def apply(stmt):
        """Applys the filter to the given sqlalchemy statement using whereclauses and joins.
        Returns the new statement. The new statment might have duplicate columns, caused by
        joins, so you must use distinct to get distinct rows, even if the passed statement
        would only return distinct rows."""
        # XXX We could maybe call distinct ourselves instead of delegating it
        # to the caller.


class FilterError(Exception):
    """Raised when a filter cannot be created."""


U = TypeVar("U")


@strawberry.input
class EnumFilter(Generic[U]):
    eq: U | None = None

    def whereclause(self, column):
        # Returns a whereclause that can be used in select.where
        # https://docs.sqlalchemy.org/en/20/core/selectable.html#sqlalchemy.sql.expression.Select.where
        expressions = []
        if self.eq is not None:
            expressions.append(column == self.eq)

        return sa.and_(*expressions) if expressions else True


T = TypeVar("T")


@strawberry.input
class ListFilter(Generic[T]):
    # Lists are used only in other non-list filters.
    # A list filter ist never on its own.
    has: T | None = None
    hasNot: T | None = None
    len: "IntFilter | None" = None

    def apply_to(self, stmt, relation):
        # XXX Performance considerations?
        # What about exists queries?
        # https://docs.sqlalchemy.org/en/20/orm/queryguide/select.html#exists-forms-has-any
        if self.has is not None:
            stmt = stmt.join(relation)
            stmt = self.has.apply(stmt)
        if self.hasNot is not None:
            raise NotImplementedError
        if self.len is not None:
            raise NotImplementedError

        return stmt


@strawberry.input
class IntIdentifierFilter:
    eq: int | None = None
    in_: list[int] | None = strawberry.field(
        name="in",
        default=None,
    )

    def whereclause(self, column):
        # Returns a whereclause that can be used in select.where
        # https://docs.sqlalchemy.org/en/20/core/selectable.html#sqlalchemy.sql.expression.Select.where
        expressions = []
        if self.eq is not None:
            expressions.append(column == self.eq)
        if self.in_ is not None:
            expressions.append(column.in_(self.in_))

        return sa.and_(*expressions) if expressions else True


@strawberry.input
class IntFilter:
    eq: int | None = None
    gt: int | None = None
    ge: int | None = None
    lt: int | None = None
    le: int | None = None
    in_: list[int] | None = strawberry.field(
        name="in",
        default=None,
    )

    def whereclause(self, column):
        expressions = []
        if self.eq is not None:
            expressions.append(column == self.eq)
        if self.gt is not None:
            expressions.append(column > self.gt)
        if self.ge is not None:
            expressions.append(column >= self.ge)
        if self.lt is not None:
            expressions.append(column < self.lt)
        if self.le is not None:
            expressions.append(column <= self.le)
        if self.in_ is not None:
            expressions.append(column.in_(self.in_))

        return sa.and_(*expressions) if expressions else True


@strawberry.input
class StringFilter:
    eq: str | None = None
    in_: list[str] | None = strawberry.field(
        name="in",
        default=None,
    )

    def whereclause(self, column):
        expressions = []
        if self.eq is not None:
            expressions.append(column == self.eq)
        if self.in_ is not None:
            expressions.append(column.in_(self.in_))

        return sa.and_(*expressions) if expressions else True


@strawberry.input
class StringRegexFilter:
    eq: str | None = None
    in_: list[str] | None = strawberry.field(
        name="in",
        default=None,
    )
    regex: str | None = None

    def whereclause(self, column):
        expressions = []
        if self.eq is not None:
            expressions.append(column == self.eq)
        if self.in_ is not None:
            expressions.append(column.in_(self.in_))
        if self.regex is not None:
            expressions.append(column.regexp_match(self.regex))

        return sa.and_(*expressions) if expressions else True


@attrs.frozen
class Filtered:
    T: type
    module_path: str
    graphql_name: str
    python_name: str

    @property
    def lazy_filter_import(self):
        """Returns the lazy import for the filter"""
        return Annotated[self.filter_name, strawberry.lazy(self.module_path)]

    @property
    def lazy_list_filter_import(self):
        """Returns the lazy import for the filter"""
        return Annotated[self.list_filter_name, strawberry.lazy(self.module_path)]

    @property
    def filter_name(self):
        """The graphql name of the filter"""
        return self.graphql_name + "Filter"

    @property
    def list_filter_name(self):
        """The graphql name of the list filter"""
        return self.graphql_name + "ListFilter"


@attrs.frozen
class FilteredType(Filtered):
    """Information about a strawberry.type decorated with add"""

    Model: type
    definition: StrawberryObjectDefinition

    @classmethod
    def from_type(cls, T: type):
        Model = inspect.get_model_or_raise(T)
        module_path = inspect.get_module_or_raise(T)
        definition = inspect.get_definition_or_raise(T)

        if definition.is_input:
            raise ValueError(
                f"{T} must be a strawberry.type or strawberry.interface, not a strawberry.input"
            )

        return cls(
            T=T,
            Model=Model,
            module_path=module_path,
            graphql_name=definition.name,
            python_name=T.__name__,
            definition=definition,
        )


class FileteredEnum(Filtered):
    @classmethod
    def from_type(cls, T: type):
        module_path = inspect.get_module_or_raise(T)
        return cls(
            T=T,
            module_path=module_path,
            graphql_name=T.__name__,
            python_name=T.__name__,
        )


# Keys are the types graphql name
_name2filtered: dict[str, Filtered] = {}


def _track(T: type):
    if has_object_definition(T):
        _track_type(T)
    elif hasattr(T, "_enum_definition"):
        _track_enum(T)
    else:
        raise FilterError(f"Given type {T} is neither a strawberry input, object, or enum.")


def _track_type(T: type):
    """Adds a type to _name2unresolved"""
    try:
        filtered_type = FilteredType.from_type(T)
    except (ValueError, TypeError) as e:
        raise FilterError(f"Can not create filter for type {T}") from e

    name = filtered_type.graphql_name
    if existing := _name2filtered.get(name, None):
        raise FilterError(
            f"There already is a type/enum decorated with @add of name {name}: {existing}"
        )

    _name2filtered[name] = filtered_type


def _track_enum(E: type):
    try:
        filtered_enum = FileteredEnum.from_type(E)
    except (ValueError, TypeError) as e:
        raise FilterError(f"Can not create filter for enum {T}") from e

    name = filtered_enum.graphql_name
    if existing := _name2filtered.get(name, None):
        raise FilterError(
            f"There already is a type/enum decorated with @add of name {name}: {existing}"
        )

    _name2filtered[name] = filtered_enum


def add(overwrite: bool = False):
    """This decorator adds a TypeFilter type to the given strawberry type called 'Filter'.
    Since filters can be used on nested types, all nested types must also be decorated with a filter.
    Otherwise, a ValueError will be raised on calling resolve.

    Call resolve, after you are done creating all filters.
    """

    def decorator(T):
        if hasattr(T, "Filter") and not overwrite:
            raise FilterError(
                f"{T} has an attribute 'Filter' which would be overwriten by {add.__qualname__}."
            )
        _track(T)

        return T

    return decorator


def resolve():
    """Must be called after all types and their dependencies are decorated with
    @add.
    If filters is not None, it must be a list of pre-existing filters.
    """
    # XXX How to make sure the filters match our api?
    # Make them inherit?
    # Why do we need it again?

    for filtered in _name2filtered.values():
        Filter = _create_filter(filtered)
        setattr(filtered.T, "Filter", Filter)

        module = importlib.import_module(filtered.module_path)
        # XXX Alternativly, we could implement our own strawberry.lazy.
        if hasattr(module, filtered.filter_name):
            raise FilterError(f"Module {module} already has an attribute {filtered.filter_name}.")
        setattr(module, filtered.filter_name, Filter)

        @strawberry.input(name=filtered.list_filter_name)
        class TListFilter(ListFilter[Filter]):
            pass

        if hasattr(module, filtered.list_filter_name):
            raise FilterError(
                f"Module {module} already has an attribute {filtered.list_filter_name}."
            )
        setattr(module, filtered.list_filter_name, TListFilter)


def _lazy_import_from_union(annotation):
    args = typing.get_args(annotation)
    if len(args) > 2:  # noqa: PLR2004
        raise NotImplementedError
    arg = args[0] or args[1]

    if isinstance(arg, str):
        return _name2filtered[arg].lazy_filter_import

    match arg:
        case types.GenericAlias:
            sub_args = typing.get_args(arg)
            if len(sub_args) > 1:
                raise NotImplementedError
            return _name2filtered[sub_args[0]].lazy_filter_import
        case _:
            return _lazy_filter_annotation_from_field_annotation(arg)


def _lazy_import_from_annotated(annotation):
    args = typing.get_args(annotation)
    if len(args) != 2:  # noqa: PLR2004
        raise NotImplementedError
    arg = args[0]

    if isinstance(arg, str):
        return _name2filtered[arg].lazy_filter_import

    match arg:
        case types.GenericAlias:
            sub_args = typing.get_args(arg)
            if len(sub_args) > 1:
                raise NotImplementedError
            return _name2filtered[sub_args[0]].lazy_filter_import
        case _:
            return _lazy_filter_annotation_from_field_annotation(arg)


def _lazy_import_from_string(annotation: str):
    match annotation:
        case "int":
            return Annotated["IntFilter", strawberry.lazy("zacken.graphql.filter")]
        case "str":
            return Annotated["StringRegexFilter", strawberry.lazy("zacken.graphql.filter")]
        case "float":
            raise NotImplementedError
        case _:
            return _name2filtered[annotation].lazy_filter_import


def _lazy_import_from_type(annotation: type):
    if annotation is str:
        return Annotated["StringRegexFilter", strawberry.lazy("zacken.graphql.filter")]
    elif annotation is int:
        return Annotated["IntFilter", strawberry.lazy("zacken.graphql.filter")]
    else:
        return _name2filtered[annotation.__name__].lazy_filter_import


def _lazy_import_from_generic_alias(annotation):
    origin = typing.get_origin(annotation)
    if origin != list:
        raise NotImplementedError

    args = typing.get_args(annotation)
    if len(args) > 1:
        raise NotImplementedError

    arg = args[0]
    assert isinstance(
        arg, str
    ), "XXX This currently does not support nested things like list[list[str] | int]"

    return _name2filtered[arg].lazy_list_filter_import


def _lazy_filter_annotation_from_field_annotation(annotation):  # noqa: PLR0911
    """Given a field annotation like Optional[BinaryLargeObject] or list[TreeArchive],
    returns the matching typing.Annotated lazy strawberry annotation for the filter.
    For the two previous examples, it would return
    Annotated["BinaryLargeObjectFilter", strawberry.lazy(".filter")],
    and
    Annotated["BinaryLargeObjectListFilter", strawberry.lazy(".filter")],
    """
    # XXX It works, but Annotated complains that it needs an expression
    match annotation:
        case str():
            return _lazy_import_from_string(annotation)
        case typing.ForwardRef():
            arg = annotation.__forward_arg__
            if not isinstance(arg, str):
                raise NotImplementedError
            return _lazy_import_from_string(arg)
        case types.GenericAlias():
            return _lazy_import_from_generic_alias(annotation)
        case types.UnionType():
            return _lazy_import_from_union(annotation)
        case typing.NewType():
            if not annotation == strawberry.ID:
                raise NotImplementedError
            # XXX This must not always we be an integer
            return Annotated["IntIdentifierFilter", strawberry.lazy("zacken.graphql.filter")]
        case type():
            return _lazy_import_from_type(annotation)
        case _:
            err = ValueError(f"Annotation {annotation} has unsupported type {type(annotation)}")
            if not (origin := typing.get_origin(annotation)):
                raise err

            if origin is typing.Union:
                return _lazy_import_from_union(annotation)
            if origin is typing.Annotated:
                return _lazy_import_from_annotated(annotation)
            else:
                raise err


def _annotation_is_optional(annotation):
    match annotation:
        case types.UnionType():
            return types.NoneType in typing.get_args(annotation)
        case _:
            if not (origin := typing.get_origin(annotation)):
                return False
            if origin is not typing.Union:
                return False

            return types.NoneType in typing.get_args(annotation)


def _create_filter(filtered: Filtered):
    match filtered:
        case FilteredType():
            return _create_type_filter(filtered)
        case FileteredEnum():
            return _create_enum_filter(filtered)
        case _:
            assert False, filtered


def _create_enum_filter(filtered_enum: FileteredEnum):
    @strawberry.input(name=filtered_enum.filter_name)
    class TFilter(EnumFilter[filtered_enum.T]):
        pass

    return TFilter


def _create_type_filter(filtered_type: FilteredType):
    # XXX Use typing.annotated to hint different filter types.
    # E.g. to specify StringRegexFilter or StringFilter.
    # In theory, we could also do this using graphql directives.
    definition = filtered_type.definition
    Model = filtered_type.Model
    inspector = sa.inspect(Model)
    columns = inspector.columns
    column_names = columns.keys()
    relationships = inspector.relationships
    relationship_names = relationships.keys()

    name2strawberryfield = {}
    annotations = {}
    name2is_optional = {}
    name2model_name = {}

    for field in definition.fields:
        if field.name not in (relationship_names + column_names) and field.base_resolver is None:
            raise ValueError(
                f"The model {Model} of {filtered_type.T} has not relationship or column corresponding to field {field.name}"
            )
        if field.type_annotation is None:
            raise ValueError(
                f"Cannot create filter for non-annotated field {field.name} on type {filtered_type.T}"
            )
        annotation = field.type_annotation.raw_annotation

        field_filter_type = _lazy_filter_annotation_from_field_annotation(annotation)
        name2strawberryfield[field.name] = strawberry.field(default=strawberry.UNSET)
        annotations[field.name] = typing.Optional[field_filter_type]
        # XXX Ensure that the corresponding column/relationship is also nullalbe
        name2is_optional[field.name] = _annotation_is_optional(annotation)
        name2model_name[field.name] = (
            field.name[len("get_") :] if field.name.startswith("get_") else field.name
        )

    def apply(self, stmt):
        for name in name2strawberryfield:
            if (filter := getattr(self, name)) is strawberry.UNSET:
                continue

            if filter is None and not name2is_optional[name]:
                continue

            model_name = name2model_name[name]
            del name

            if model_name in relationship_names:
                relationship = getattr(Model, model_name)
                if filter is None:
                    stmt = stmt.where(relationship != None)  # noqa: E711
                elif isinstance(filter, ListFilter):
                    stmt = filter.apply_to(stmt, relationship)
                else:
                    stmt = stmt.join(getattr(Model, model_name))
                    stmt = filter.apply(stmt)
            elif model_name in column_names:
                column = getattr(Model, model_name)
                if filter is None:
                    stmt = stmt.where(column != None)  # noqa: E711
                else:
                    stmt = stmt.where(filter.whereclause(column))

        return stmt

    def exec_body(ns):
        ns.update(name2strawberryfield)
        ns.update({"__annotations__": annotations})
        ns.update({"apply": apply})

    Filter = strawberry.input(
        types.new_class(
            name=definition.name + "Filter",
            bases=(TypeFilter,),
            exec_body=exec_body,
        )
    )

    return Filter
