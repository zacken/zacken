# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

import os
import pathlib as pl

import graphql
from graphql.language import (
    EnumTypeDefinitionNode,
    EnumValueDefinitionNode,
    FieldDefinitionNode,
    InterfaceTypeDefinitionNode,
    ObjectTypeDefinitionNode,
    TypeDefinitionNode,
)
from strawberry.field import StrawberryField
from strawberry.type import get_object_definition
from strawberry.utils.str_converters import to_camel_case

_rewrite_sdl = os.getenv("ZACKEN_REWRITE_SDL") is not None

_schema_path = pl.Path(__file__).parent / "schema.graphql"
_schema_str = _schema_path.read_text()
_ast = graphql.parse(_schema_str)

# Keys are graphql types or fields in dottet notation
# Query
# Query.getDevices
_descriptions = {}


def _add_descriptions_from_type_node(node: TypeDefinitionNode):
    if node.description is not None:
        _descriptions[node.name.value] = node.description.value

    match node:
        case ObjectTypeDefinitionNode():
            for field in node.fields:
                _add_description_from_field_node(field, node)
        case InterfaceTypeDefinitionNode():
            for field in node.fields:
                _add_description_from_field_node(field, node)
        case EnumTypeDefinitionNode():
            for value in node.values:
                _add_description_from_enum_value_definition_node(value, node)


def _add_description_from_enum_value_definition_node(
    node: EnumValueDefinitionNode, parent: EnumTypeDefinitionNode
):
    if node.description is None:
        return

    _descriptions[f"{parent.name.value}.{node.name.value}"] = node.description.value


def _add_description_from_field_node(node: FieldDefinitionNode, parent: TypeDefinitionNode):
    if node.description is None:
        return

    _descriptions[f"{parent.name.value}.{node.name.value}"] = node.description.value


def _populate_descriptions():
    for definition in _ast.definitions:
        match definition:
            case TypeDefinitionNode():
                _add_descriptions_from_type_node(definition)


_populate_descriptions()


def description_for(identifier: str) -> str | None:
    if not (description := _descriptions.get(identifier)):
        if _rewrite_sdl:
            return None
        raise ValueError(f"There is no description for {identifier} in {_schema_path}")

    return description


def describe_field(typename: str, name: str | None = None):
    """Add the description from the GraphQL SDL file to the decorated strawberry field."""

    def _decorator(field: StrawberryField):
        field_name = None
        if name is not None:
            field_name = name
        elif field.name is not None:
            field_name = name
        elif field.base_resolver is not None:
            field_name = field.base_resolver.name
        else:
            raise ValueError(
                "The field has neither a name, nor a resolver, please use the name attribute to give it"
            )

        field.description = description_for(f"{typename}.{to_camel_case(field_name)}")

        return field

    return _decorator


def describe_type(T):
    """Add the description from the GraphQL SDL file to the decorated strawberry type."""
    if not (definition := get_object_definition(T)):
        raise ValueError(f"{T} is neigher a strawberry type, interface, or enum.")

    definition.description = description_for(definition.name)
    return T
