interface Archive {
  """XXX Add description"""
  id: ID!

  """The archives kind."""
  kind: ArchiveKind!
}

input ArchiveFilter {
  id: IntIdentifierFilter
  kind: ArchiveKindFilter
}

enum ArchiveKind {
  Tree
  Unknown
  Transforming
}

input ArchiveKindFilter {
  eq: ArchiveKind = null
}

input ArchiveListFilter {
  has: ArchiveFilter = null
  hasNot: ArchiveFilter = null
  len: IntFilter = null
}

"""A contiguous piece of binary data."""
type BinaryLargeObject {
  """XXX Add description"""
  id: ID!

  """XXX Add description"""
  ref: String!

  """XXX Add description"""
  size: Int!

  """The unpacking report of this blob."""
  unpackingReport: UnpackingReport!

  """XXX Add description"""
  analysisReport: BinaryLargeObjectAnalysisReport!
}

"""XXX Add description"""
type BinaryLargeObjectAnalysisReport {
  id: ID!

  """XXX Add description"""
  blob: BinaryLargeObject!

  """XXX Add description"""
  softwareSignatureStatus: SchedulingStatus

  """XXX Add description"""
  softwareSignature: SoftwareSignature
}

input BinaryLargeObjectAnalysisReportFilter {
  id: IntIdentifierFilter
  blob: BinaryLargeObjectFilter
  softwareSignatureStatus: SchedulingStatusFilter
  softwareSignature: SoftwareSignatureFilter
}

input BinaryLargeObjectFilter {
  id: IntIdentifierFilter
  ref: StringRegexFilter
  size: IntFilter
  unpackingReport: UnpackingReportFilter
  analysisReport: BinaryLargeObjectAnalysisReportFilter
}

input BinaryLargeObjectListFilter {
  has: BinaryLargeObjectFilter = null
  hasNot: BinaryLargeObjectFilter = null
  len: IntFilter = null
}

"""XXX Add description"""
type Chunk {
  """XXX Add description"""
  id: ID!

  """The blob that is this chunk"""
  blob: BinaryLargeObject!

  """
  The offset that this chunk is located in
  the unknownArchive's blob.
  """
  offset: Int!

  """The length of the chunk."""
  length: Int!

  """The archive that contains this chunk"""
  unknownArchive: UnknownArchive!
}

input ChunkFilter {
  id: IntIdentifierFilter
  blob: BinaryLargeObjectFilter
  offset: IntFilter
  length: IntFilter
  unknownArchive: UnknownArchiveFilter
}

input ChunkListFilter {
  has: ChunkFilter = null
  hasNot: ChunkFilter = null
  len: IntFilter = null
}

"""
An embedded device as defined by Muench et al. [1].

[1] M. Muench, J. Stijohann, F. Kargl, A. Francillon, and D. Balzarotti,
    “What You Corrupt Is Not What You Crash: Challenges in Fuzzing Embedded
    Devices,” in Proceedings 2018 Network and Distributed System Security
    Symposium, San Diego, CA: Internet Society, 2018. doi: 10.14722/ndss.2018.23166.
"""
type Device {
  """XXX Add description"""
  id: ID!

  """XXX Add description"""
  getFirmwareImages: [FirmwareImage!]
}

input DeviceFilter {
  id: IntIdentifierFilter
  getFirmwareImages: FirmwareImageListFilter
}

"""XXX Add description"""
type Directory implements FileSystemNode {
  """XXX Add description"""
  id: ID!

  """The node's kind"""
  kind: FileSystemNodeKind!

  """The node's name"""
  name: String!

  """
  The node's path in the tree. These filepaths resemble unix filesystem
  paths and use '/' as a separator.
  """
  path: String!

  """The direct children of the directory."""
  getChildren(filter: FileSystemNodeFilter = null): [FileSystemNode!]!
}

"""XXX Add description"""
type File implements FileSystemNode {
  """XXX Add description"""
  id: ID!

  """The node's kind"""
  kind: FileSystemNodeKind!

  """The node's name"""
  name: String!

  """
  The node's path in the tree. These filepaths resemble unix filesystem
  paths and use '/' as a separator.
  """
  path: String!

  """The blob associated with this file."""
  blob: BinaryLargeObject!
}

interface FileSystemNode {
  """XXX Add description"""
  id: ID!

  """The node's kind"""
  kind: FileSystemNodeKind!

  """The node's name"""
  name: String!

  """
  The node's path in the tree. These filepaths resemble unix filesystem
  paths and use '/' as a separator.
  """
  path: String!
}

input FileSystemNodeFilter {
  id: IntIdentifierFilter
  kind: FileSystemNodeKindFilter
  name: StringRegexFilter
  path: StringRegexFilter
}

enum FileSystemNodeKind {
  File
  SymbolicLink
  Directory
}

input FileSystemNodeKindFilter {
  eq: FileSystemNodeKind = null
}

input FileSystemNodeListFilter {
  has: FileSystemNodeFilter = null
  hasNot: FileSystemNodeFilter = null
  len: IntFilter = null
}

"""
A firmware image as defined by Costin et al. [1].
Firmware images can be aquired from various sources [2]:
* Using debug interfaces to read memory contents
* Dumping contents of flash chips
* Utilizing software methods (e.g. downloading from the internet)

[1] A. Costin, A. Zarras, and A. Francillon, “Towards Automated
    Classification of Firmware Images and Identification of Embedded Devices,” in
    ICT Systems Security and Privacy Protection, vol. 502, S. De Capitani Di
    Vimercati and F. Martinelli, Eds., in IFIP Advances in Information and
    Communication Technology, vol. 502. , Cham: Springer International Publishing,
    2017, pp. 233–247. doi: 10.1007/978-3-319-58469-0_16.
[2] S. Vasile, D. Oswald, and T. Chothia, “Breaking All the Things—A
    Systematic Survey of Firmware Extraction Techniques for IoT Devices,” in
    Smart Card Research and Advanced Applications, B. Bilgin and J.-B. Fischer,
    Eds., in Lecture Notes in Computer Science. Cham: Springer International
    Publishing, 2019, pp. 171–185. doi: 10.1007/978-3-030-15462-2_12.
"""
type FirmwareImage {
  """XXX Add description"""
  id: ID!

  """The blob that represents the images data"""
  blob: BinaryLargeObject!

  """
  The device that associated with this firmware image.
  Can be null to allow unkonwn devices which happens e.g. when downloading
  images automatically from the internet.
  """
  device: Device

  """XXX Add description"""
  getBlobs(filter: BinaryLargeObjectFilter = null): [BinaryLargeObject!]!

  """XXX Add description"""
  getArchives(filter: ArchiveFilter = null): [Archive!]!

  """XXX Add description"""
  getFileSystemNodes(filter: FileSystemNodeFilter = null): [FileSystemNode!]!

  """XXX Add description"""
  getChunks(filter: ChunkFilter = null): [Chunk!]
}

input FirmwareImageFilter {
  id: IntIdentifierFilter
  blob: BinaryLargeObjectFilter
  device: DeviceFilter
  getBlobs: BinaryLargeObjectListFilter
  getArchives: ArchiveListFilter
  getFileSystemNodes: FileSystemNodeListFilter
  getChunks: ChunkListFilter
}

input FirmwareImageListFilter {
  has: FirmwareImageFilter = null
  hasNot: FirmwareImageFilter = null
  len: IntFilter = null
}

input IntFilter {
  eq: Int = null
  gt: Int = null
  ge: Int = null
  lt: Int = null
  le: Int = null
  in: [Int!] = null
}

input IntIdentifierFilter {
  eq: Int = null
  in: [Int!] = null
}

type LinuxSignatures {
  id: Int!
  linuxBanner: Int
  linuxProcBanner: Int
  rtcYdays: Int
  rtcDaysInMonth: Int
  Ctype: Int
  hexAsc: Int
  hexAscUpper: Int
  bandTable: Int
  bannerStr: String
}

input LinuxSignaturesFilter {
  id: IntFilter
  linuxBanner: IntFilter
  linuxProcBanner: IntFilter
  rtcYdays: IntFilter
  rtcDaysInMonth: IntFilter
  Ctype: IntFilter
  hexAsc: IntFilter
  hexAscUpper: IntFilter
  bandTable: IntFilter
  bannerStr: StringRegexFilter
}

"""
The root query type.
Zacken supports complex search queries using all edges starting with 'get'.
For example Query.getFirmwares allows to query all FirmwareImage based on
attributes and relatons.
Edges not starting with 'get' allow little to no filtering.
"""
type Query {
  """XXX Add description"""
  getFirmwareImages(filter: FirmwareImageFilter = null): [FirmwareImage!]!

  """XXX Add description"""
  getBlobAnalysisReports(filter: BinaryLargeObjectAnalysisReportFilter = null): [BinaryLargeObjectAnalysisReport!]!

  """XXX Add description"""
  getDevices(filter: DeviceFilter = null): [Device!]!
}

enum SchedulingStatus {
  Skipped
  Pending
  Failed
  Retry
  Done
}

input SchedulingStatusFilter {
  eq: SchedulingStatus = null
}

type SoftwareSignature {
  id: ID!
  linux: LinuxSignatures
}

input SoftwareSignatureFilter {
  id: IntIdentifierFilter
  linux: LinuxSignaturesFilter
}

input StringRegexFilter {
  eq: String = null
  in: [String!] = null
  regex: String = null
}

"""XXX Add description"""
type SymbolicLink implements FileSystemNode {
  """XXX Add description"""
  id: ID!

  """The node's kind"""
  kind: FileSystemNodeKind!

  """The node's name"""
  name: String!

  """
  The node's path in the tree. These filepaths resemble unix filesystem
  paths and use '/' as a separator.
  """
  path: String!

  """The path to where the link points"""
  dest: String!
}

"""
A TransformingArchive transforms a single blob to another single blob.
Examples include compression, encryption, encoding, and, as an specific example,
device-trees.
"""
type TransformingArchive implements Archive {
  """XXX Add description"""
  id: ID!

  """The archives kind."""
  kind: ArchiveKind!

  """XXX Add description"""
  transformed: BinaryLargeObject!
}

"""XXX Add description"""
type TreeArchive implements Archive {
  """XXX Add description"""
  id: ID!

  """The archives kind."""
  kind: ArchiveKind!
  getNodes(filter: FileSystemNodeFilter = null): [FileSystemNode!]!
}

type Unblob {
  id: ID!
  status: SchedulingStatus!
  archive: Archive!
}

input UnblobFilter {
  id: IntIdentifierFilter
  status: SchedulingStatusFilter
  archive: ArchiveFilter
}

"""
A blob that is not of any known archive format is carved for interesting
contents.
"""
type UnknownArchive implements Archive {
  """XXX Add description"""
  id: ID!

  """The archives kind."""
  kind: ArchiveKind!

  """XXX Add description"""
  getChunks(filter: ChunkFilter = null): [Chunk!]!
}

input UnknownArchiveFilter {
  id: IntIdentifierFilter
  kind: ArchiveKindFilter
  getChunks: ChunkListFilter
}

"""An unpacking report contains all informatoin related to unpacking."""
type UnpackingReport {
  """XXX Add description"""
  id: ID!

  """The blob that is being unpacked"""
  blob: BinaryLargeObject!

  """XXX Add description"""
  unblob: Unblob
}

input UnpackingReportFilter {
  id: IntIdentifierFilter
  blob: BinaryLargeObjectFilter
  unblob: UnblobFilter
}