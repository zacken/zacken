# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

import abc
import typing

import attrs
import sqlalchemy as sa
import sqlalchemy.orm
from strawberry import LazyType
from strawberry.field import StrawberryField
from strawberry.type import (
    StrawberryContainer,
    StrawberryType,
)
from strawberry.types.nodes import (
    FragmentSpread,
    InlineFragment,
    SelectedField,
    Selection,
)
from strawberry.types.types import StrawberryObjectDefinition
from strawberry.utils.str_converters import to_camel_case

from . import inspect


class FieldNotLoadedError(Exception):
    pass


Product = typing.TypeVar("Product")


class TypeFactory(typing.Generic[Product], metaclass=abc.ABCMeta):
    """A factory that can be used to produce instances of the strawberry.type Product
    from a sqlalchemy model.
    See add for more informaton about the model.
    Factories are mainly used to avoid lazy evaluation of a model's relationships.
    Further, they use sqlalchemy.orm.load_only only load the requested fields.
    See the `options` method for more informaton.
    """

    @classmethod
    @abc.abstractmethod
    def from_selections(
        cls,
        selections: list[Selection],
    ) -> "TypeFactory":
        """Given a strawberry.types.nodes.SelectedField that is a selection of
        Product, return a TypeFactory that produces products that contain exactly
        the fields of the selection."""

    @abc.abstractmethod
    def produce(self, model) -> Product:
        """Produces an instance of U, given a model.
        Raises FieldNotLoadedError if the attribute is not loaded in the model."""

    def load_options(self) -> list[sa.orm.Load] | None:
        """Returns a list of load options that can be applied to the product's
        model to load exactly the selected fields. Note that the load options
        load the whole tree, given by the selections. Thus it suffices to only
        load the product's model with the corresponding load options.
        """
        options = []
        if load_only := self._load_only_columns():
            options.extend(load_only)
        if selectinload := self._selectinload_relationships():
            options.extend(selectinload)

        if len(options) == 0:
            return None

        return options

    @abc.abstractmethod
    def _selectinload_relationships(self) -> list[sa.orm.Load] | None:
        """Returns a list of `sa.orm.selectinload` load options.
        Each load option corresponds to a relationship of the product's model.
        Relationships of polymorphic types are included. Note that the
        returned load options are already told to selectinload and load_only
        the respective relationship's model relationships and columns."""

    @abc.abstractmethod
    def _load_only_columns(self) -> list[sa.orm.Load] | None:
        """Returns a list of `sa.orm.load_only` load options.
        Each load option corresponds to a column of the product's model.
        Relationships of polymorphic types are included."""

    @property
    @abc.abstractmethod
    def _produced_type(self) -> "ProducedType":
        pass


class FactoryError(Exception):
    """Raise when a factory cannot be created"""


def add(overwrite=False):
    """This decorator marks the decorated type to be a Product,
    that can be produced by a 'Factory'.
    After all product are decorated, call resolve, to expose the factory thes
    as 'Factory' on the respective types.
    """

    def decorator(T):
        if hasattr(T, "Factory") and not overwrite:
            raise FactoryError(
                f"{T} has an attribute 'Factory' which would be overwriten by {add.__qualname__}."
            )

        try:
            produced_type = ProducedType.from_type(T)
        except (ValueError, TypeError) as e:
            raise FactoryError from e

        _graphql_name2produced[produced_type.graphql_name] = produced_type
        _model_name2produced[produced_type.Model.__name__] = produced_type

        return T

    return decorator


def resolve():
    """ "Adds a Factory attribute to all types decorated with @add."""
    for produced_type in _graphql_name2produced.values():
        Factory = _create_factory(produced_type)
        setattr(produced_type.T, "Factory", Factory)
        # XXX Test that all subtypes have a factory


class _NotSet:
    pass


@attrs.frozen
class ProducedType:
    # XXX Document that models have to match strawberry.types perfectly.
    T: type
    Model: type
    definition: StrawberryObjectDefinition
    graphql_name: str
    python_name: str
    #: Dictionary mapping graphql field names
    unresolved_fields: dict[str, StrawberryField]
    #: A dicitonary with all fields, mapping graphql names to python names
    graphql_name2python_name: dict[str, str]

    @classmethod
    def from_type(cls, T: type):
        Model = inspect.get_model_or_raise(T)
        definition = inspect.get_definition_or_raise(T)

        if definition.is_input:
            raise ValueError(
                f"{T} must be a strawberry.type or strawberry.interface, not a strawberry.input"
            )

        graphql_name2python_name = {}
        unresolved_fields: dict[str, StrawberryField] = {}
        for field in definition.fields:
            graphql_name = _graphql_name_from_field(field)
            graphql_name2python_name[graphql_name] = field.python_name
            if field.base_resolver is None:
                unresolved_fields[graphql_name] = field

        return cls(
            T=T,
            Model=Model,
            graphql_name=definition.name,
            python_name=T.__name__,
            definition=definition,
            unresolved_fields=unresolved_fields,
            graphql_name2python_name=graphql_name2python_name,
        )

    @property
    def inspector(self):
        # Everything derived from the inspector is first needed when resolve
        # is called.
        # Further, creating the inspector requires the sqlalchemy models to be
        # fully imported. If an analyzer/unpacker decorates their types using
        # factory.add and the inspector was called, an sqlalchemy error would
        # be raised, as the analyzer's models are not yet fully imported.
        return sa.inspect(self.Model)

    @property
    def pk_columns(self):
        return {pk.name for pk in self.inspector.primary_key}

    @property
    def relationships(self):
        return self.inspector.relationships

    @property
    def columns(self):
        return {column.name for column in self.inspector.columns}

    @property
    def factory_name(self):
        return self.graphql_name + "Factory"


_graphql_name2produced: dict[str, ProducedType] = {}
# XXX Remove this and use only _graphql_name2produced
_model_name2produced: dict[str, ProducedType] = {}


def _get_or_raise(model, name: str):
    value = model.__dict__.get(name, _NotSet)
    if value == _NotSet:
        raise FieldNotLoadedError(
            f"{name} is not loaded on object {model.__repr__()} of type {type(model)}"
        )

    return value


def _produce_or_none(factory, model):
    if model is None:
        return None
    return factory.produce(model)


def _graphql_name_from_field(field: StrawberryField):
    # XXX There must be a more correct way to get the graphql name
    # from a field.
    # Maybe there is not because we only know how to convert the names
    # when calling strawberry.Schema
    if field.graphql_name is not None:
        return field.graphql_name

    return to_camel_case(field.name)


def _peel_strawberry_container(field_type: StrawberryType) -> StrawberryType:
    """Recursivly peels all containers from a type and returns only the innermost container"""
    if not isinstance(field_type, StrawberryContainer):
        return field_type

    return _peel_strawberry_container(field_type.of_type)


# For factories for strawberry.type's, we can just instanciate the type from the given model.
# For strawberry.interface's this is harder since we cannot instanciate the type directly.
# Thus, we use this dictionary to get the correct type to instanciate.
_modeltype2strawberrytype = {}


@attrs.frozen
class LoadedRelationship:
    # The attribute that you use in sqlalchemy statements.
    # This is what you get when you do e.g. Unblob.archive.
    instrumented: sa.orm.InstrumentedAttribute
    # All load options that should be applied to the loaded relationship.
    # E.g. using load_only on the model's columns.
    load_options: list[sa.orm.Load] | None
    # The type of the relationship that should be loaded
    model_type: type
    # All subtypes of the relationship that should also be loaded
    model_subtypes: list[type] | None

    def selectinload(self) -> sa.orm.Load:
        """Returns a sa.orm.selectinload load option that loads this
        relationship.
        The load_option are applied as suboptions.
        """
        ret = sa.orm.selectinload(self.instrumented)
        if self.model_subtypes:
            ret = ret.selectin_polymorphic(self.model_subtypes)

        if self.load_options:
            ret = ret.options(*self.load_options)

        return ret


def _create_factory(produced_type: ProducedType) -> type:
    Model = produced_type.Model

    class TFactory(TypeFactory[produced_type.T]):
        def __init__(self, scalar_fields, object_factories, subtype_object_factories):
            self._scalar_fields: set = scalar_fields
            self._object_factories: dict[str, TypeFactory] = object_factories
            self._subtype_object_factories: dict[str, TypeFactory] = subtype_object_factories

        @property
        def _produced_type(self) -> ProducedType:
            return produced_type

        @classmethod
        def from_selections(
            cls,
            selections: list[Selection],
        ):
            # All scalar fields of the factories product that are selected.
            # XXX Maybe add subtype_scalar_fields
            scalar_fields = set()
            # All factories for objects related via an edge.
            # XXX Rename to relationship_object_factories
            object_factories: dict[str, TypeFactory] = {}
            # Factories for all subtypes of the factories product.
            subtype_object_factories: dict[str, TypeFactory] = {}

            for selection in selections:
                match selection:
                    case SelectedField():
                        if selection.name not in produced_type.graphql_name2python_name:
                            raise ValueError(
                                f"{selection.name} is not a field of {produced_type.T}"
                            )
                        if not (field := produced_type.unresolved_fields.get(selection.name)):
                            continue

                        if field.python_name in produced_type.relationships:
                            Child = _peel_strawberry_container(field.type)
                            if isinstance(Child, LazyType):
                                # XXX Performance considerations?
                                Child = Child.resolve_type()
                            # This works because this is only called after the factories have been added
                            # in resolve.
                            object_factories[field.python_name] = Child.Factory.from_selections(
                                selection.selections
                            )
                        elif field.python_name in produced_type.columns:
                            scalar_fields.add(field.python_name)
                        else:
                            assert False, "XXX Model verification"
                    case FragmentSpread() | InlineFragment():
                        subtype = selection.type_condition
                        produced_subtype = _graphql_name2produced[subtype]
                        subtype_object_factories[subtype] = (
                            produced_subtype.T.Factory.from_selections(selection.selections)
                        )

            for primary_key_name in produced_type.pk_columns:
                scalar_fields.add(primary_key_name)

            return cls(
                scalar_fields=scalar_fields,
                object_factories=object_factories,
                subtype_object_factories=subtype_object_factories,
            )

        def _load_only_columns(self) -> list[sa.orm.Load] | None:
            columns = []
            for field in self._scalar_fields:
                assert (
                    field in produced_type.columns
                ), "XXX The mapping between model is currently not checked"
                columns.append(getattr(Model, field))

            # XXX This is technically wrong.
            # selectin_polymorphic selects ALL polymorphic fields of the given types.
            # Somehow I cannot manage to apply load_only to the resutling options to.
            # Note that simply specifying the load_option for the polymorphic subtype
            # in the one options call does not work.
            subtype_models = []
            for subtype_factory in self._subtype_object_factories.values():
                subtype_models.append(subtype_factory._produced_type.Model)

            if len(columns) == 0 and len(subtype_models) == 0:
                return None

            ret = sa.orm.load_only(*columns)

            if len(subtype_models) != 0:
                ret = ret.selectin_polymorphic(subtype_models)

            return [ret]

        def _selectinload_relationships(self) -> list[sa.orm.Load] | None:
            field2loaded_relationship: dict[str, LoadedRelationship] = {}

            for field, field_factory in self._object_factories.items():
                assert (
                    field in produced_type.relationships
                ), "XXX validate model against strawberry type"

                field2loaded_relationship[field] = LoadedRelationship(
                    instrumented=getattr(Model, field),
                    load_options=field_factory.load_options(),
                    model_type=Model,
                    model_subtypes=[
                        factory._produced_type.Model
                        for factory in field_factory._subtype_object_factories.values()
                    ],
                )

            subtype_selectinload = []
            for subtype_factory in self._subtype_object_factories.values():
                if not (subtype_options := subtype_factory._selectinload_relationships()):
                    continue
                subtype_selectinload.extend(subtype_options)

            if len(field2loaded_relationship) == 0 and len(subtype_selectinload) == 0:
                return None

            return [
                loaded_rel.selectinload() for loaded_rel in field2loaded_relationship.values()
            ] + subtype_selectinload

        def produce(self, model):
            # XXX These python_names must not always match the models
            # We should allow to give some kind of mapper dict
            # whose keys are strawberry type names
            # and whose values are strings or functions.
            # A string value would simply indicate that this strawberry field
            # is named differntly in the sqlalchemy model.
            # A function would build the strawberry field from the model.
            kwargs = {}
            for python_name in self._scalar_fields:
                kwargs[python_name] = _get_or_raise(model, python_name)

            for python_name in self._object_factories:
                child_model = _get_or_raise(model, python_name)
                kwargs[python_name] = _produce_or_none(
                    self._object_factories[python_name], child_model
                )

            model_produced = _model_name2produced[type(model).__name__]
            for subtype_factory in self._subtype_object_factories.values():
                if subtype_factory._produced_type != model_produced:
                    continue

                for python_name in subtype_factory._scalar_fields:
                    kwargs[python_name] = _get_or_raise(model, python_name)

                for python_name in subtype_factory._object_factories:
                    child_model = _get_or_raise(model, python_name)
                    kwargs[python_name] = _produce_or_none(
                        subtype_factory._object_factories[python_name], child_model
                    )

            for field in model_produced.graphql_name2python_name.values():
                # XXX This is a hack. We nedd this as we do not know the
                # unresolved fields of a potential child type.
                if field.startswith("get_"):
                    continue
                kwargs.setdefault(field, None)

            return model_produced.T(**kwargs)

    _modeltype2strawberrytype[Model] = produced_type.T
    return TFactory
