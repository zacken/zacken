# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

import collections

import attrs
import sqlalchemy_utils as sau

from . import models as m


@attrs.frozen
class NodeId:
    lpath: str

    @classmethod
    def from_str(cls, lpath: str) -> "NodeId":
        return cls(
            lpath=lpath,
        )

    @classmethod
    def from_ltree(cls, ltree: sau.Ltree) -> "NodeId":
        return cls(
            lpath=ltree.path,
        )

    def to_ltree(self) -> sau.Ltree:
        return sau.Ltree(self.lpath)


@attrs.frozen
class TreeArchiveBuilderNode:
    node: m.FileSystemNode
    """None if the archive is the parent"""
    parent_id: NodeId | None


class TreeArchiveBuilder:
    """Adds nodes with the correct lpath, not more not less.
    Also adds the archive tree to the nodes."""

    def __init__(self):
        self._archive = m.TreeArchive()
        self._level_node_counts: dict[int, int] = collections.defaultdict(lambda: 0)
        self._nodes: dict[NodeId, m.FileSystemNode] = {}

    def _get_lpath_and_path(self, builder_node: TreeArchiveBuilderNode) -> tuple[sau.Ltree, str]:
        if builder_node.parent_id is None:
            level = 1
            level_node_count = self._level_node_counts[level]
            self._level_node_counts[level] += 1
            return (sau.Ltree(str(level_node_count)), builder_node.node.name)
        else:
            parent = self._nodes[builder_node.parent_id]
            # +1 to match the behavior of the postgres ltree function nlevel
            parent_level = parent.lpath.path.count(".") + 1
            level = parent_level + 1
            level_node_count = self._level_node_counts[level]

            self._level_node_counts[level] += 1
            return (
                parent.lpath + sau.Ltree(str(level_node_count)),
                parent.path + "/" + builder_node.node.name,
            )

    def add(self, builder_node: TreeArchiveBuilderNode) -> NodeId:
        """Returns the node id the created node"""
        if builder_node.node.lpath is not None:
            raise ValueError("lpath must be None")

        if builder_node.node.path is not None:
            raise ValueError("path must be None")

        if builder_node.node.name is None:
            raise ValueError("name must be set")

        if builder_node.parent_id is not None:
            parent = self._nodes[builder_node.parent_id]
            if not isinstance(parent, m.Directory):
                raise ValueError(
                    f"The parent of {builder_node} has a parent of invalid type {type(parent)}"
                )

        node = builder_node.node
        node.tree_archive = self._archive
        node.lpath, node.path = self._get_lpath_and_path(builder_node)

        node_id = NodeId.from_ltree(node.lpath)

        self._nodes[node_id] = node

        return node_id

    def finalize(self) -> m.TreeArchive:
        return self._archive
