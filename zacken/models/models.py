# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

import enum

import sqlalchemy as sa
import sqlalchemy_utils as sau
from sqlalchemy import ForeignKey
from sqlalchemy.ext.asyncio import AsyncAttrs
from sqlalchemy.orm import (
    DeclarativeBase,
    Mapped,
    mapped_column,
    relationship,
)

from zacken.types import Ref


def get_not_null_constraint(type: str, column: str):
    """Single table inheritance requires to define all columns as nullable.
    This constraint ensures that they are still not null.
    Quoting from [1]
    > PostgreSQL does not support CHECK constraints that reference table data
    > other than the new or updated row being checked. While a CHECK constraint
    > that violates this rule may appear to work in simple tests, it cannot
    > guarantee that the database will not reach a state in which the
    > constraint condition is false (due to subsequent changes of the other
    > row(s) involved).

    [1]: https://www.postgresql.org/docs/current/ddl-constraints.html
    """
    return sa.CheckConstraint(
        f"(type != '{type}') OR (type = '{type}' AND \"{column}\" IS NOT NULL)",
        name=f"check_{type}_{column}_not_null",
    )


def _is_single_table_inheritance(cls):
    cls_tablename = cls.__tablename__

    for base in cls.__bases__:
        if cls_tablename == getattr(base, "__tablename__", None):
            return True

    return False


# For why we need AsyncAttrs see [1]
# [1] https://docs.sqlalchemy.org/en/20/orm/extensions/asyncio.html#preventing-implicit-io-when-using-asyncsession
class Base(AsyncAttrs, DeclarativeBase):
    pass


class SchedulingStatus(enum.Enum):
    """The status of a task. Inspired by clery states.
    https://docs.celeryq.dev/en/stable/userguide/tasks.html#built-in-states
    """

    # The task was skipped by task specific logic
    Skipped = "Skipped"
    # The task is pending
    Pending = "Pending"
    # The execution failed and the task is not being retried
    Failed = "Failed"
    # The task failed and is being retried
    Retry = "Retry"
    # The task finished successfully
    Done = "Done"


# XXX Device and FirmwareImage should have a many-to-many relation
class Device(Base):
    __tablename__ = "device"

    id: Mapped[int] = mapped_column(
        primary_key=True,
        index=True,
    )


class FirmwareImage(Base):
    __tablename__ = "firmware_image"

    id: Mapped[int] = mapped_column(
        primary_key=True,
        index=True,
    )

    blob_id: Mapped[int] = mapped_column(
        ForeignKey("blob.id"),
        index=True,
        # XXX We somehow need this due to the  post_update=True in image
        # The examples do not need this. Maybe the reason ist that we have a
        # circle with more than two nodes.
        nullable=True,
    )
    blob: Mapped["BinaryLargeObject"] = relationship(
        foreign_keys=blob_id,
        # Break the dependency cycle caused by HasBinaryLargeObject.
        # For example:
        # Firmware->FirmwareImage->BinaryLargeObject->Firmware
        post_update=True,
    )

    device_id: Mapped[int] = mapped_column(
        ForeignKey("device.id"),
        nullable=True,
    )
    device: Mapped[Device] = relationship(
        foreign_keys=device_id,
    )

    # To avoid complicated queries, we saved the firmware id
    # in everyting that is revevant for quering.
    # XXX Add checks that validate this
    # As an alternative say that this may not be set manually
    # but rather by some code that I still have to write
    archives: Mapped[list["Archive"]] = relationship(
        back_populates="firmware_image",
        foreign_keys="Archive.firmware_image_id",
    )
    blobs: Mapped[list["BinaryLargeObject"]] = relationship(
        back_populates="firmware_image",
        foreign_keys="BinaryLargeObject.firmware_image_id",
    )
    file_system_nodes: Mapped[list["FileSystemNode"]] = relationship(
        back_populates="firmware_image",
        foreign_keys="FileSystemNode.firmware_image_id",
    )
    chunks: Mapped[list["Chunk"]] = relationship(
        back_populates="firmware_image",
        foreign_keys="Chunk.firmware_image_id",
    )
    unpacking_reports: Mapped[list["UnpackingReport"]] = relationship(
        back_populates="firmware_image",
        foreign_keys="UnpackingReport.firmware_image_id",
    )


class HasFirmwareImageMixin:
    """This mixin can be used for everyting that
    has a one to many relationship with firmware.
    It back_populates the cls.__tablename__, so be sure to add the relation to firmware"""

    # XXX Add triggers that check this relationship.
    # Note that we cannot use check constraints because the constraint spans over multiple tables.
    # https://www.postgresql.org/docs/current/ddl-constraints.html#DDL-CONSTRAINTS-CHECK-CONSTRAINTS

    @sa.orm.declared_attr
    def firmware_image_id(cls) -> Mapped[int]:
        # XXX deduplicate this
        single_table_inheritance = _is_single_table_inheritance(cls)
        return mapped_column(
            ForeignKey(
                "firmware_image.id",
                name=f"fk_{cls.__tablename__}_firmware_image_id:{cls.__name__}",
            ),
            index=True,
            use_existing_column=single_table_inheritance,
            nullable=single_table_inheritance,
        )

    @sa.orm.declared_attr
    def firmware_image(cls) -> Mapped[FirmwareImage]:
        # XXX If we were really smart, we could infer the firmware using clientside sql expressions
        # https://docs.sqlalchemy.org/en/20/core/defaults.html
        return relationship(
            foreign_keys=cls.firmware_image_id,
            back_populates=cls.__tablename__ + "s",
        )


class BinaryLargeObject(Base, HasFirmwareImageMixin):
    """BinaryLargeObject represents a set of contigous bytes.
    It owns an unpacking report and the binary data analysis reports.
    The unpacking report in turn owns the archive"""

    __tablename__ = "blob"

    id: Mapped[int] = mapped_column(
        primary_key=True,
        index=True,
    )

    ref: Mapped[Ref] = mapped_column(
        # XXX Make this unique.
        # This is hard since we do not want to make the reference the primary key
        # as we believe (but do not know for sure) that this would waste space
        # in tables that reference this table.
    )

    size: Mapped[int] = mapped_column()

    # Both the unpacking_report and the analysis_report must be set on this
    # class. They are not nullable.
    # The idea is that even if the analysis/unpacking did not run we have
    # something to report: nothing was done yet.
    unpacking_report_id: Mapped[int] = mapped_column(
        ForeignKey("unpacking_report.id"),
    )
    unpacking_report: Mapped["UnpackingReport"] = relationship(
        foreign_keys=unpacking_report_id,
        back_populates="blob",
    )
    analysis_report_id: Mapped[int] = mapped_column(
        ForeignKey("blob_analysis_report.id"),
    )
    analysis_report: Mapped["BinaryLargeObjectAnalysisReport"] = relationship(
        foreign_keys=analysis_report_id,
        back_populates="blob",
    )

    def __init__(self, *, ref: Ref, size: int):
        self.ref = ref
        self.size = size
        self.analysis_report = BinaryLargeObjectAnalysisReport()
        self.unpacking_report = UnpackingReport()


class HasBinaryLargeObject:
    @sa.orm.declared_attr
    def blob_id(cls) -> Mapped[int]:
        # XXX improve this
        single_table_inheritance = _is_single_table_inheritance(cls)
        return mapped_column(
            ForeignKey("blob.id"),
            index=True,
            use_existing_column=single_table_inheritance,
            nullable=single_table_inheritance,
        )

    @sa.orm.declared_attr
    def blob(cls) -> Mapped[BinaryLargeObject]:
        return relationship(
            foreign_keys=cls.blob_id,
        )


class ArchiveKind(enum.Enum):
    Tree = "Tree"
    Unknown = "Unknown"
    Transforming = "Transforming"


class Archive(Base, HasFirmwareImageMixin):
    __tablename__ = "archive"
    __table_args__ = tuple()
    __mapper_args__ = {
        "polymorphic_abstract": True,
        "polymorphic_on": "kind",
    }
    kind: Mapped[ArchiveKind] = mapped_column(
        sa.Enum(ArchiveKind),
        index=True,
    )

    id: Mapped[int] = mapped_column(
        primary_key=True,
        index=True,
    )


class TreeArchive(Archive):
    __mapper_args__ = {
        "polymorphic_identity": ArchiveKind.Tree,
    }
    nodes: Mapped[list["FileSystemNode"]] = relationship(
        back_populates="tree_archive",
    )


class FileSystemNodeKind(enum.Enum):
    File = "File"
    SymbolicLink = "SymbolicLink"
    Directory = "Directory"


class FileSystemNode(Base, HasFirmwareImageMixin):
    __tablename__ = "file_system_node"
    __mapper_args__ = {
        "polymorphic_on": "kind",
        "polymorphic_abstract": True,
    }
    __table_args__ = (
        sa.Index(
            "ix_file_system_node_lpath",
            "lpath",
            postgresql_using="gist",
        ),
    )
    kind: Mapped[FileSystemNodeKind] = mapped_column(
        sa.Enum(FileSystemNodeKind),
        index=True,
    )

    id: Mapped[int] = mapped_column(
        primary_key=True,
        index=True,
    )

    # Do not set manually, use tree_archive_builder
    lpath: Mapped[sau.LtreeType] = mapped_column(
        sau.LtreeType,
    )
    # Readonly
    children: Mapped[list["FileSystemNode"]] = relationship(
        primaryjoin="func.subpath(remote(FileSystemNode.lpath), 0, -1) == foreign(FileSystemNode.lpath)",
        backref=sa.orm.backref("parent", uselist=False),
        viewonly=True,
        uselist=True,
    )
    name: Mapped[str] = mapped_column()
    # Do not set manually, use tree_archive_builder
    path: Mapped[str] = mapped_column()

    # XXX Add constraint
    tree_archive_id: Mapped[int] = mapped_column(
        ForeignKey(
            "archive.id",
            name="fk_file_system_node_archive_id",
        ),
        index=True,
    )
    tree_archive: Mapped[TreeArchive] = relationship(
        foreign_keys=tree_archive_id,
        back_populates="nodes",
    )

    def __str__(self) -> str:
        return f"{type(self).__qualname__}({self.path})"


class File(FileSystemNode, HasBinaryLargeObject):
    __mapper_args__ = {
        "polymorphic_identity": FileSystemNodeKind.File,
    }
    FileSystemNode.__table_args__ += (
        get_not_null_constraint("file", "size"),
        get_not_null_constraint("file", "blob_id"),
    )


class SymbolicLink(FileSystemNode):
    __mapper_args__ = {
        "polymorphic_identity": FileSystemNodeKind.SymbolicLink,
    }
    FileSystemNode.__table_args__ += (
        get_not_null_constraint("symbolic_link", "name"),
        get_not_null_constraint("symbolic_link", "dest"),
    )
    dest: Mapped[str] = mapped_column(
        nullable=True,
    )


class Directory(FileSystemNode):
    __mapper_args__ = {
        "polymorphic_identity": FileSystemNodeKind.Directory,
    }


class TransformingArchive(Archive):
    __mapper_args__ = {
        "polymorphic_identity": ArchiveKind.Transforming,
    }
    Archive.__table_args__ += (get_not_null_constraint("transforming_archive", "transformed_id"),)
    transformed_id: Mapped[int] = mapped_column(
        sa.ForeignKey("blob.id"),
        nullable=True,
    )
    transformed: Mapped[BinaryLargeObject] = relationship(
        foreign_keys=transformed_id,
    )


class UnknownArchive(Archive):
    __mapper_args__ = {
        "polymorphic_identity": ArchiveKind.Unknown,
    }
    chunks: Mapped[list["Chunk"]] = relationship(
        back_populates="unknown_archive",
    )


class Chunk(Base, HasBinaryLargeObject, HasFirmwareImageMixin):
    __tablename__ = "chunk"

    id: Mapped[int] = mapped_column(
        primary_key=True,
        index=True,
    )

    # XXX add trigger that verifies that this is actually an unknown archive
    unknown_archive_id: Mapped[int] = mapped_column(
        ForeignKey(
            "archive.id",
            name="fk_chunk_archive_id",
        ),
        index=True,
    )
    unknown_archive: Mapped[UnknownArchive] = relationship(
        foreign_keys=unknown_archive_id,
        back_populates="chunks",
    )

    offset: Mapped[int] = mapped_column()
    length: Mapped[int] = mapped_column()


class UnpackingReport(Base, HasFirmwareImageMixin):
    __tablename__ = "unpacking_report"

    id: Mapped[int] = mapped_column(
        primary_key=True,
        index=True,
    )
    blob: Mapped[BinaryLargeObject] = relationship(
        back_populates="unpacking_report",
    )
    unblob_id: Mapped[int] = mapped_column(
        ForeignKey("unblob.id", name="fk_unpacking_report_unblob"),
        nullable=True,
        index=True,
    )
    unblob = relationship(
        "Unblob",
        foreign_keys=unblob_id,
    )


class BinaryLargeObjectAnalysisReport(Base):
    __tablename__ = "blob_analysis_report"
    id: Mapped[int] = mapped_column(
        primary_key=True,
    )

    blob: Mapped["BinaryLargeObject"] = relationship(
        back_populates="analysis_report",
    )

    software_signature_id: Mapped[int] = mapped_column(
        ForeignKey("software_signature.id"),
        nullable=True,
        index=True,
    )
    # Null if the task was not scheduled
    software_signature_status = mapped_column(
        sa.Enum(SchedulingStatus),
        nullable=True,
        index=True,
    )
    software_signature = relationship(
        "SoftwareSignature",
        foreign_keys=software_signature_id,
        back_populates="report",
    )


class BinaryLargeObjectAnalysisMixin:
    @sa.orm.declared_attr
    def report(cls) -> Mapped["BinaryLargeObjectAnalysisReport"]:
        return relationship(
            "BinaryLargeObjectAnalysisReport",
            back_populates=cls.__tablename__,
        )
