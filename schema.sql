-- SPDX-FileCopyrightText: 2024 Marten Ringwelski
-- SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
--
-- SPDX-License-Identifier: AGPL-3.0-only

--
-- PostgreSQL database dump
--

-- Dumped from database version 16.0 (Debian 16.0-1.pgdg120+1)
-- Dumped by pg_dump version 16.0 (Debian 16.0-1.pgdg120+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

-- *not* creating schema, since initdb creates it


ALTER SCHEMA public OWNER TO postgres;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS '';


--
-- Name: ltree; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS ltree WITH SCHEMA public;


--
-- Name: EXTENSION ltree; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION ltree IS 'data type for hierarchical tree-like structures';


--
-- Name: archivekind; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.archivekind AS ENUM (
    'Tree',
    'Unknown',
    'Transforming'
);


ALTER TYPE public.archivekind OWNER TO postgres;

--
-- Name: filesystemnodekind; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.filesystemnodekind AS ENUM (
    'File',
    'SymbolicLink',
    'Directory'
);


ALTER TYPE public.filesystemnodekind OWNER TO postgres;

--
-- Name: schedulingstatus; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.schedulingstatus AS ENUM (
    'Skipped',
    'Pending',
    'Failed',
    'Retry',
    'Done'
);


ALTER TYPE public.schedulingstatus OWNER TO postgres;

--
-- Name: unpacker; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.unpacker AS ENUM (
    'unblob',
    'FACT',
    'Zacken',
    'binwalk'
);


ALTER TYPE public.unpacker OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: archive; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.archive (
    kind public.archivekind NOT NULL,
    id integer NOT NULL,
    firmware_image_id integer NOT NULL,
    transformed_id integer
);


ALTER TABLE public.archive OWNER TO postgres;

--
-- Name: archive_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.archive_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.archive_id_seq OWNER TO postgres;

--
-- Name: archive_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.archive_id_seq OWNED BY public.archive.id;


--
-- Name: blob; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.blob (
    id integer NOT NULL,
    ref character varying NOT NULL,
    size integer NOT NULL,
    unpacking_report_id integer NOT NULL,
    analysis_report_id integer NOT NULL,
    firmware_image_id integer NOT NULL
);


ALTER TABLE public.blob OWNER TO postgres;

--
-- Name: blob_analysis_report; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.blob_analysis_report (
    id integer NOT NULL,
    software_signature_id integer,
    software_signature_status public.schedulingstatus
);


ALTER TABLE public.blob_analysis_report OWNER TO postgres;

--
-- Name: blob_analysis_report_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.blob_analysis_report_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.blob_analysis_report_id_seq OWNER TO postgres;

--
-- Name: blob_analysis_report_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.blob_analysis_report_id_seq OWNED BY public.blob_analysis_report.id;


--
-- Name: blob_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.blob_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.blob_id_seq OWNER TO postgres;

--
-- Name: blob_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.blob_id_seq OWNED BY public.blob.id;


--
-- Name: chunk; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.chunk (
    id integer NOT NULL,
    unknown_archive_id integer NOT NULL,
    "offset" integer NOT NULL,
    length integer NOT NULL,
    blob_id integer NOT NULL,
    firmware_image_id integer NOT NULL
);


ALTER TABLE public.chunk OWNER TO postgres;

--
-- Name: chunk_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.chunk_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.chunk_id_seq OWNER TO postgres;

--
-- Name: chunk_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.chunk_id_seq OWNED BY public.chunk.id;


--
-- Name: device; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.device (
    id integer NOT NULL
);


ALTER TABLE public.device OWNER TO postgres;

--
-- Name: device_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.device_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.device_id_seq OWNER TO postgres;

--
-- Name: device_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.device_id_seq OWNED BY public.device.id;


--
-- Name: file_system_node; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.file_system_node (
    kind public.filesystemnodekind NOT NULL,
    id integer NOT NULL,
    lpath public.ltree NOT NULL,
    name character varying NOT NULL,
    path character varying NOT NULL,
    tree_archive_id integer NOT NULL,
    firmware_image_id integer NOT NULL,
    blob_id integer,
    dest character varying
);


ALTER TABLE public.file_system_node OWNER TO postgres;

--
-- Name: file_system_node_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.file_system_node_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.file_system_node_id_seq OWNER TO postgres;

--
-- Name: file_system_node_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.file_system_node_id_seq OWNED BY public.file_system_node.id;


--
-- Name: firmware_image; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.firmware_image (
    id integer NOT NULL,
    blob_id integer,
    device_id integer
);


ALTER TABLE public.firmware_image OWNER TO postgres;

--
-- Name: firmware_image_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.firmware_image_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.firmware_image_id_seq OWNER TO postgres;

--
-- Name: firmware_image_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.firmware_image_id_seq OWNED BY public.firmware_image.id;


--
-- Name: linux_signatures; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.linux_signatures (
    id integer NOT NULL,
    linux_banner integer,
    linux_proc_banner integer,
    rtc_ydays integer,
    rtc_days_in_month integer,
    _ctype integer,
    hex_asc integer,
    hex_asc_upper integer,
    band_table integer,
    banner_str character varying
);


ALTER TABLE public.linux_signatures OWNER TO postgres;

--
-- Name: linux_signatures_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.linux_signatures_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.linux_signatures_id_seq OWNER TO postgres;

--
-- Name: linux_signatures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.linux_signatures_id_seq OWNED BY public.linux_signatures.id;


--
-- Name: software_signature; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.software_signature (
    id integer NOT NULL,
    linux_id integer
);


ALTER TABLE public.software_signature OWNER TO postgres;

--
-- Name: software_signature_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.software_signature_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.software_signature_id_seq OWNER TO postgres;

--
-- Name: software_signature_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.software_signature_id_seq OWNED BY public.software_signature.id;


--
-- Name: unblob; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.unblob (
    id integer NOT NULL,
    status public.schedulingstatus NOT NULL,
    archive_id integer
);


ALTER TABLE public.unblob OWNER TO postgres;

--
-- Name: unblob_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.unblob_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.unblob_id_seq OWNER TO postgres;

--
-- Name: unblob_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.unblob_id_seq OWNED BY public.unblob.id;


--
-- Name: unpacking_report; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.unpacking_report (
    id integer NOT NULL,
    unblob_id integer,
    firmware_image_id integer NOT NULL
);


ALTER TABLE public.unpacking_report OWNER TO postgres;

--
-- Name: unpacking_report_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.unpacking_report_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.unpacking_report_id_seq OWNER TO postgres;

--
-- Name: unpacking_report_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.unpacking_report_id_seq OWNED BY public.unpacking_report.id;


--
-- Name: archive id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.archive ALTER COLUMN id SET DEFAULT nextval('public.archive_id_seq'::regclass);


--
-- Name: blob id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.blob ALTER COLUMN id SET DEFAULT nextval('public.blob_id_seq'::regclass);


--
-- Name: blob_analysis_report id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.blob_analysis_report ALTER COLUMN id SET DEFAULT nextval('public.blob_analysis_report_id_seq'::regclass);


--
-- Name: chunk id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chunk ALTER COLUMN id SET DEFAULT nextval('public.chunk_id_seq'::regclass);


--
-- Name: device id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.device ALTER COLUMN id SET DEFAULT nextval('public.device_id_seq'::regclass);


--
-- Name: file_system_node id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.file_system_node ALTER COLUMN id SET DEFAULT nextval('public.file_system_node_id_seq'::regclass);


--
-- Name: firmware_image id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.firmware_image ALTER COLUMN id SET DEFAULT nextval('public.firmware_image_id_seq'::regclass);


--
-- Name: linux_signatures id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.linux_signatures ALTER COLUMN id SET DEFAULT nextval('public.linux_signatures_id_seq'::regclass);


--
-- Name: software_signature id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.software_signature ALTER COLUMN id SET DEFAULT nextval('public.software_signature_id_seq'::regclass);


--
-- Name: unblob id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unblob ALTER COLUMN id SET DEFAULT nextval('public.unblob_id_seq'::regclass);


--
-- Name: unpacking_report id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unpacking_report ALTER COLUMN id SET DEFAULT nextval('public.unpacking_report_id_seq'::regclass);


--
-- Name: archive archive_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.archive
    ADD CONSTRAINT archive_pkey PRIMARY KEY (id);


--
-- Name: blob_analysis_report blob_analysis_report_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.blob_analysis_report
    ADD CONSTRAINT blob_analysis_report_pkey PRIMARY KEY (id);


--
-- Name: blob blob_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.blob
    ADD CONSTRAINT blob_pkey PRIMARY KEY (id);


--
-- Name: chunk chunk_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chunk
    ADD CONSTRAINT chunk_pkey PRIMARY KEY (id);


--
-- Name: device device_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.device
    ADD CONSTRAINT device_pkey PRIMARY KEY (id);


--
-- Name: file_system_node file_system_node_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.file_system_node
    ADD CONSTRAINT file_system_node_pkey PRIMARY KEY (id);


--
-- Name: firmware_image firmware_image_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.firmware_image
    ADD CONSTRAINT firmware_image_pkey PRIMARY KEY (id);


--
-- Name: linux_signatures linux_signatures_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.linux_signatures
    ADD CONSTRAINT linux_signatures_pkey PRIMARY KEY (id);


--
-- Name: software_signature software_signature_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.software_signature
    ADD CONSTRAINT software_signature_pkey PRIMARY KEY (id);


--
-- Name: unblob unblob_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unblob
    ADD CONSTRAINT unblob_pkey PRIMARY KEY (id);


--
-- Name: unpacking_report unpacking_report_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unpacking_report
    ADD CONSTRAINT unpacking_report_pkey PRIMARY KEY (id);


--
-- Name: ix_archive_firmware_image_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_archive_firmware_image_id ON public.archive USING btree (firmware_image_id);


--
-- Name: ix_archive_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_archive_id ON public.archive USING btree (id);


--
-- Name: ix_archive_kind; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_archive_kind ON public.archive USING btree (kind);


--
-- Name: ix_blob_analysis_report_software_signature_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_blob_analysis_report_software_signature_id ON public.blob_analysis_report USING btree (software_signature_id);


--
-- Name: ix_blob_analysis_report_software_signature_status; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_blob_analysis_report_software_signature_status ON public.blob_analysis_report USING btree (software_signature_status);


--
-- Name: ix_blob_firmware_image_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_blob_firmware_image_id ON public.blob USING btree (firmware_image_id);


--
-- Name: ix_blob_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_blob_id ON public.blob USING btree (id);


--
-- Name: ix_chunk_blob_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_chunk_blob_id ON public.chunk USING btree (blob_id);


--
-- Name: ix_chunk_firmware_image_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_chunk_firmware_image_id ON public.chunk USING btree (firmware_image_id);


--
-- Name: ix_chunk_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_chunk_id ON public.chunk USING btree (id);


--
-- Name: ix_chunk_unknown_archive_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_chunk_unknown_archive_id ON public.chunk USING btree (unknown_archive_id);


--
-- Name: ix_device_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_device_id ON public.device USING btree (id);


--
-- Name: ix_file_system_node_blob_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_file_system_node_blob_id ON public.file_system_node USING btree (blob_id);


--
-- Name: ix_file_system_node_firmware_image_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_file_system_node_firmware_image_id ON public.file_system_node USING btree (firmware_image_id);


--
-- Name: ix_file_system_node_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_file_system_node_id ON public.file_system_node USING btree (id);


--
-- Name: ix_file_system_node_kind; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_file_system_node_kind ON public.file_system_node USING btree (kind);


--
-- Name: ix_file_system_node_lpath; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_file_system_node_lpath ON public.file_system_node USING gist (lpath);


--
-- Name: ix_file_system_node_tree_archive_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_file_system_node_tree_archive_id ON public.file_system_node USING btree (tree_archive_id);


--
-- Name: ix_firmware_image_blob_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_firmware_image_blob_id ON public.firmware_image USING btree (blob_id);


--
-- Name: ix_firmware_image_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_firmware_image_id ON public.firmware_image USING btree (id);


--
-- Name: ix_linux_signatures_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_linux_signatures_id ON public.linux_signatures USING btree (id);


--
-- Name: ix_software_signature_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_software_signature_id ON public.software_signature USING btree (id);


--
-- Name: ix_software_signature_linux_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_software_signature_linux_id ON public.software_signature USING btree (linux_id);


--
-- Name: ix_unblob_archive_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_unblob_archive_id ON public.unblob USING btree (archive_id);


--
-- Name: ix_unblob_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_unblob_id ON public.unblob USING btree (id);


--
-- Name: ix_unblob_status; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_unblob_status ON public.unblob USING btree (status);


--
-- Name: ix_unpacking_report_firmware_image_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_unpacking_report_firmware_image_id ON public.unpacking_report USING btree (firmware_image_id);


--
-- Name: ix_unpacking_report_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_unpacking_report_id ON public.unpacking_report USING btree (id);


--
-- Name: ix_unpacking_report_unblob_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_unpacking_report_unblob_id ON public.unpacking_report USING btree (unblob_id);


--
-- Name: archive archive_transformed_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.archive
    ADD CONSTRAINT archive_transformed_id_fkey FOREIGN KEY (transformed_id) REFERENCES public.blob(id);


--
-- Name: blob blob_analysis_report_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.blob
    ADD CONSTRAINT blob_analysis_report_id_fkey FOREIGN KEY (analysis_report_id) REFERENCES public.blob_analysis_report(id);


--
-- Name: blob_analysis_report blob_analysis_report_software_signature_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.blob_analysis_report
    ADD CONSTRAINT blob_analysis_report_software_signature_id_fkey FOREIGN KEY (software_signature_id) REFERENCES public.software_signature(id);


--
-- Name: blob blob_unpacking_report_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.blob
    ADD CONSTRAINT blob_unpacking_report_id_fkey FOREIGN KEY (unpacking_report_id) REFERENCES public.unpacking_report(id);


--
-- Name: chunk chunk_blob_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chunk
    ADD CONSTRAINT chunk_blob_id_fkey FOREIGN KEY (blob_id) REFERENCES public.blob(id);


--
-- Name: file_system_node file_system_node_blob_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.file_system_node
    ADD CONSTRAINT file_system_node_blob_id_fkey FOREIGN KEY (blob_id) REFERENCES public.blob(id);


--
-- Name: firmware_image firmware_image_blob_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.firmware_image
    ADD CONSTRAINT firmware_image_blob_id_fkey FOREIGN KEY (blob_id) REFERENCES public.blob(id);


--
-- Name: firmware_image firmware_image_device_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.firmware_image
    ADD CONSTRAINT firmware_image_device_id_fkey FOREIGN KEY (device_id) REFERENCES public.device(id);


--
-- Name: archive fk_archive_firmware_image_id:Archive; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.archive
    ADD CONSTRAINT "fk_archive_firmware_image_id:Archive" FOREIGN KEY (firmware_image_id) REFERENCES public.firmware_image(id);


--
-- Name: blob fk_blob_firmware_image_id:BinaryLargeObject; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.blob
    ADD CONSTRAINT "fk_blob_firmware_image_id:BinaryLargeObject" FOREIGN KEY (firmware_image_id) REFERENCES public.firmware_image(id);


--
-- Name: chunk fk_chunk_archive_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chunk
    ADD CONSTRAINT fk_chunk_archive_id FOREIGN KEY (unknown_archive_id) REFERENCES public.archive(id);


--
-- Name: chunk fk_chunk_firmware_image_id:Chunk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chunk
    ADD CONSTRAINT "fk_chunk_firmware_image_id:Chunk" FOREIGN KEY (firmware_image_id) REFERENCES public.firmware_image(id);


--
-- Name: file_system_node fk_file_system_node_archive_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.file_system_node
    ADD CONSTRAINT fk_file_system_node_archive_id FOREIGN KEY (tree_archive_id) REFERENCES public.archive(id);


--
-- Name: file_system_node fk_file_system_node_firmware_image_id:FileSystemNode; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.file_system_node
    ADD CONSTRAINT "fk_file_system_node_firmware_image_id:FileSystemNode" FOREIGN KEY (firmware_image_id) REFERENCES public.firmware_image(id);


--
-- Name: unpacking_report fk_unpacking_report_firmware_image_id:UnpackingReport; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unpacking_report
    ADD CONSTRAINT "fk_unpacking_report_firmware_image_id:UnpackingReport" FOREIGN KEY (firmware_image_id) REFERENCES public.firmware_image(id);


--
-- Name: unpacking_report fk_unpacking_report_unblob; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unpacking_report
    ADD CONSTRAINT fk_unpacking_report_unblob FOREIGN KEY (unblob_id) REFERENCES public.unblob(id);


--
-- Name: software_signature software_signature_linux_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.software_signature
    ADD CONSTRAINT software_signature_linux_id_fkey FOREIGN KEY (linux_id) REFERENCES public.linux_signatures(id);


--
-- Name: unblob unblob_archive_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unblob
    ADD CONSTRAINT unblob_archive_id_fkey FOREIGN KEY (archive_id) REFERENCES public.archive(id);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE USAGE ON SCHEMA public FROM PUBLIC;


--
-- PostgreSQL database dump complete
--

