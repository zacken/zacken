<!--
SPDX-FileCopyrightText: 2024 Marten Ringwelski
SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Development
```
uvicorn zacken.graphql.app:app --reload
ZACKEN_DEBUG_SQL
ZACKEN_REWRITE_SDL
ZACKEN_CONFIG_PATH
pre-commit install --hook-type commit-msg

docker run \
   -p 9000:9000 \
   -p 9001:9001 \
   --name zacken-minio \
   -v /var/lib/zacken/minio-data/:/data \
   -e "MINIO_ROOT_USER=root" \
   -e "MINIO_ROOT_PASSWORD=rootroot" \
   quay.io/minio/minio server /data --console-address ":9001"
docker run --name zacken-postgres -e POSTGRES_PASSWORD=postgres -p 5432:5432 -d postgres:alpine
docker run -p 6379:6379 --name zacken-redis -d redis
docker run -d -p 5672:5672 --name zacken-rabbitmq rabbitmq

CREATE DATABASE zacken;
\c zacken

docker exec zacken-postgres pg_dump --data-only --disable-triggers --format=tar -U postgres zacken > zacken.tar
gzip --keep --decompress cases/zacken.tar.gz
docker cp cases/zacken.tar zacken-postgres:/

# Now create the things
./zacken-initdb --postgres-url 'postgresql://postgres:postgres@localhost:5432/zacken-test'

# Disable triggers to avoid failing foreign key constraings due to circular dependencies
# that sqlalchemy resolves automatcially.
docker exec -it zacken-postgres pg_restore --disable-triggers --data-only -d zacken-test -U postgres /zacken.tar
```
