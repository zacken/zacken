<!--
SPDX-FileCopyrightText: 2024 Marten Ringwelski
SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Zacken's Architecture
This document gives a high level overview of Zacken's internal architecture.
Zacken is split in multiple loosely coupled modules.
The tree most important modules are:
- `zacken.models`: Zacken's database models.
- `zacken.graphql`: All code related to serving
- `zacken.unpacker`: All code related to unpackers, including the unpackers themselves
- `zacken.analyzer`: All code related to analyzers, including the analyzers themselves
- `zacken.worker`: A celery worker.

The modules `zacken.graphql` and `zacken.worker` both expose an app,
the former a WSGI app and the latter `celery` app.
